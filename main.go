package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strconv"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/utils/wsutil"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/logger"

	"gitlab.com/diamondburned/nixie/commands/aocboard"
	"gitlab.com/diamondburned/nixie/commands/buttkick"
	"gitlab.com/diamondburned/nixie/commands/choose"
	"gitlab.com/diamondburned/nixie/commands/expref"
	"gitlab.com/diamondburned/nixie/commands/playground"
	"gitlab.com/diamondburned/nixie/commands/roles"
	"gitlab.com/diamondburned/nixie/commands/smartlink"
	"gitlab.com/diamondburned/nixie/commands/snail"
	"gitlab.com/diamondburned/nixie/commands/stalin"
	"gitlab.com/diamondburned/nixie/commands/starboard"
	"gitlab.com/diamondburned/nixie/commands/tags"
	"gitlab.com/diamondburned/nixie/commands/verifier"

	// Playground imports
	_ "gitlab.com/diamondburned/nixie/commands/playground/golang"
	_ "gitlab.com/diamondburned/nixie/commands/playground/rextester"
	_ "gitlab.com/diamondburned/nixie/commands/playground/rust"
)

const databasePath = "_data/"
const ownerID discord.UserID = 170132746042081280

func main() {
	token := os.Getenv("BOT_TOKEN")
	if token == "" {
		log.Fatalln("$BOT_TOKEN empty")
	}

	if _, err := os.Stat(token); err == nil {
		f, err := ioutil.ReadFile(token)
		if err != nil {
			log.Fatalln("Can't read token file:", err)
		}

		token = string(f)
	}

	debug := log.New(os.Stderr, "WS-DEBUG:", 0)
	wsutil.WSDebug = func(v ...interface{}) {
		_, fn, line, _ := runtime.Caller(1)
		debug.Println(append([]interface{}{fn + ":" + strconv.Itoa(line)}, v...)...)
	}

	os.MkdirAll(databasePath, 0750)

	log.SetFlags(log.Llongfile | log.Ltime | log.Ldate)
	log.SetPrefix("\n  ")

	kv, err := db.NewKVFile(databasePath + "upmo.db")
	if err != nil {
		log.Fatalln("Failed to create a database:", err)
	}
	defer kv.Close()

	tagsdb, err := db.NewKVFile(databasePath + "tags.db")
	if err != nil {
		log.Fatalln("Failed to create the tags database:", err)
	}
	defer tagsdb.Close()

	cmds := &Commands{}

	wait, err := bot.Start(token, cmds, func(ctx *bot.Context) error {
		return mainCtx(ctx, kv, tagsdb)
	})

	if err != nil {
		log.Fatalln("Bot cannot start:", err)
	}

	log.Println("Bot started")

	if err := wait(); err != nil {
		log.Fatalln("Bot failed:", err)
	}
}

func mainCtx(ctx *bot.Context, kv, tagsdb *db.KV) error {
	// Add the Guilds intent for additional state events.
	ctx.AddIntents(gateway.IntentGuilds)

	u, err := ctx.Me()
	if err != nil {
		return errors.Wrap(err, "failed to get current user")
	}

	ctx.HasPrefix = bot.NewPrefix(
		"!",
		fmt.Sprintf("<@%d> ", u.ID),
		fmt.Sprintf("<@!%d> ", u.ID),
	)

	ctx.FormatError = func(err error) string {
		return "Error: " + err.Error()
	}

	// If there is not a valid command, then don't say anything.  We'd want to
	// tell the user that a subcommand is wrong when the command is right.
	ctx.SilentUnknown.Command = true

	// Treat message edits the same as message creates for convenience.
	ctx.EditableCommands = true

	// Use a separate database for tags.
	ctx.MustRegisterSubcommand(tags.New(tagsdb.Node("tags")))

	// Share the same bot database.
	ctx.MustRegisterSubcommand(starboard.New(kv.Node("starboard")))
	ctx.MustRegisterSubcommand(roles.New(kv.Node("guild_roles")))
	ctx.MustRegisterSubcommand(aocboard.New(kv.Node("aocboard")), "aoc")
	ctx.MustRegisterSubcommand(stalin.New(kv.Node("stalin")))
	ctx.MustRegisterSubcommand(buttkick.New(kv.Node("buttkick")))
	ctx.MustRegisterSubcommand(verifier.New(kv.Node("verifier")))
	ctx.MustRegisterSubcommand(snail.New(kv.Node("snail")))
	ctx.MustRegisterSubcommand(logger.Init(kv.Node("logger")))
	ctx.MustRegisterSubcommand(expref.New(kv.Node("expref")), "expref")
	ctx.MustRegisterSubcommand(smartlink.New())
	ctx.MustRegisterSubcommand(playground.New())
	ctx.MustRegisterSubcommand(choose.New())

	return nil
}
