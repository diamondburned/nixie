module gitlab.com/diamondburned/nixie

go 1.13

require (
	github.com/DataDog/zstd v1.4.5 // indirect
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/davecgh/go-spew v1.1.1
	github.com/dgraph-io/badger/v2 v2.0.3
	github.com/dgraph-io/ristretto v0.0.3 // indirect
	github.com/dgryski/go-farm v0.0.0-20200201041132-a6ae2369ad13 // indirect
	github.com/diamondburned/arikawa/v2 v2.0.5-0.20210218190821-a939a26cab0f
	github.com/diamondburned/dgwidgets v0.0.0-20210410190525-fdfbf87dee13
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mtibben/confusables v0.0.0-20190424120906-fbd3b71e4957
	github.com/pkg/errors v0.9.1
	github.com/tkuchiki/parsetime v0.0.0-20160330064430-5c0a0e326cdd
	gitlab.com/diamondburned/clocker v0.0.0-20190409150028-8b1f16fd9670
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
