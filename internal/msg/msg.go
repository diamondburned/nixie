package msg

import "github.com/diamondburned/arikawa/v2/api"

func SendData(content string) *api.SendMessageData {
	return &api.SendMessageData{
		Content:         content,
		AllowedMentions: &api.AllowedMentions{},
	}
}
