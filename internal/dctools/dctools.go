package dctools

import (
	"bytes"
	"strings"
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/state"
	"github.com/diamondburned/dgwidgets"
)

func ApplyMiddlewares(sub *bot.Subcommand, m interface{}, names ...interface{}) {
	for _, name := range names {
		sub.AddMiddleware(name, m)
	}
}

func Typing(ctx *bot.Context, ch discord.ChannelID, context string) func() time.Duration {
	ctx.Typing(ch)
	if context == "" {
		return nil
	}
	t := time.Now()
	return func() time.Duration {
		return time.Now().Sub(t)
	}
}

func IsGuild(m *gateway.MessageCreateEvent) error {
	if m.GuildID.IsValid() {
		return nil
	}

	return bot.Break
}

// SplitText splits the text blob to 2000-length chunks maximum.
func SplitText(input string) []string {
	var lines = strings.Split(input, "\n")

	var count int

	var buf bytes.Buffer

	var filtered = lines[:0]

	for _, line := range lines {
		// Overflow check.
		if buf.Len()+len(line) > 2000 {
			filtered = append(filtered, buf.String())
			buf.Reset()
			count = 0

			buf.WriteRune('\u200b') // preserve indentation
		}

		buf.WriteString(line)
		buf.WriteByte('\n')
		count += len(line) + 1
	}

	// Flush.
	if buf.Len() > 0 {
		filtered = append(filtered, buf.String())
	}

	if len(filtered) == 1 && filtered[0] == "" {
		return []string{input}
	}

	return filtered
}

func DefaultPaginator(s *state.State, m gateway.MessageCreateEvent) *dgwidgets.Paginator {
	p := dgwidgets.NewPaginator(s, m.ChannelID)
	p.Loop = false
	p.DeleteReactionsWhenDone = true
	p.ColourWhenDone = 0xFF0000
	p.SetTimeout(2 * time.Minute)
	p.Widget.UserWhitelist = []discord.UserID{m.Author.ID}

	return p
}

func Sanitize(content string) string {
	return EscapeCodeBlock(EscapeMentions(StrictSanitize(content)))
}

func StrictSanitize(content string) string {
	return strings.ToValidUTF8(content, "?")
}

func EscapeMentions(content string) string {
	return strings.Replace(content, "@", "@\u200b", -1)
}

var codeblockEscaper = strings.NewReplacer(
	"```", "`\u200b`\u200b`\u200b",
)

// EscapeCodeBlock escapes the codeblocks to sanittize output.
func EscapeCodeBlock(cb string) string {
	return codeblockEscaper.Replace(cb)
}

func WrapCodeBlock(content, lang string) string {
	return "```" + lang + "\n" + codeblockEscaper.Replace(content) + "```"
}

type Role string

func (r *Role) Parse(arg string) error {
	*r = Role(arg)
	return nil
}

func (r *Role) Usage() string {
	return "role"
}

func (r Role) Search(ctx *bot.Context, guild discord.GuildID) *discord.Role {
	return SearchRole(ctx, guild, string(r))
}

func SearchRole(ctx *bot.Context, guild discord.GuildID, role string) *discord.Role {
	roles, err := ctx.Roles(guild)
	if err != nil {
		return nil
	}

	var target *discord.Role
	for _, r := range roles {
		if r.Name == role || r.ID.String() == role {
			target = &r
			break
		}
	}

	return target
}

type Duration time.Duration

func (d *Duration) Parse(arg string) error {
	duration, err := time.ParseDuration(arg)
	if err != nil {
		return err
	}

	*d = Duration(duration)
	return nil
}
