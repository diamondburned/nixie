package main

import (
	"fmt"
	"runtime"
	"runtime/debug"
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

type Commands struct {
	Ctx *bot.Context
}

func (c *Commands) Setup(sub *bot.Subcommand) {
	sub.FindCommand("Help").Description = "Print this help."
	sub.Hide(c.GC)
	sub.ChangeCommandInfo(c.GC, "gc", "???")
	sub.FindCommand(c.Help).Arguments[0].String = "[subcommand]"
}

func (c *Commands) Help(m *gateway.MessageCreateEvent, f arguments.Flag) error {
	var help string
	var showHidden bool

	fs := arguments.NewFlagSet()
	fs.BoolVar(&showHidden, "all", false, "Show hidden commands")

	if err := f.With(fs.FlagSet); err != nil {
		return errors.Wrap(err, "invalid usage")
	}

	p, _ := c.Ctx.Permissions(m.ChannelID, m.Author.ID)
	showHidden = showHidden && p.Has(discord.PermissionAdministrator)

	switch len(fs.Args()) {
	case 0:
		help = contextHelp(c.Ctx, showHidden)
	case 1:
		help = subcommandHelp(c.Ctx.Subcommands(), fs.Arg(0), showHidden)
	case 2:
		return errors.New("unexpected arguments given")
	}

	if help == "" {
		return errors.New("unknown subcommand")
	}

	for i, blob := range dctools.SplitText(help) {
		if _, err := c.Ctx.SendText(m.ChannelID, blob); err != nil {
			return errors.Wrapf(err, "failed to send blob num %d", i)
		}
	}

	return nil
}

var startupTime = time.Now()

func (c *Commands) GC(m *gateway.MessageCreateEvent) error {
	if m.Author.ID != ownerID {
		return errors.New("Access denied.")
	}

	e := discord.Embed{
		Title: "Go debug info",
		Fields: []discord.EmbedField{
			embedMakeField("Number of goroutines",
				runtime.NumGoroutine(), false),
			embedMakeField("GOMAXPROCS", runtime.GOMAXPROCS(-1), true),
			embedMakeField("GOOS", runtime.GOOS, true),
			embedMakeField("GOARCH", runtime.GOARCH, true),
			embedMakeField("Go version", runtime.Version(), false),
		},
	}

	var gc debug.GCStats
	debug.ReadGCStats(&gc)

	echo := c.Ctx.Gateway.PacerLoop.EchoBeat.Get()
	sent := c.Ctx.Gateway.PacerLoop.SentBeat.Get()

	gatewayLatency := time.Duration(echo - sent)

	e.Fields = append(e.Fields,
		embedMakeField("Started time",
			startupTime.Format(time.Stamp), false),
		embedMakeField("Last garbage collection",
			gc.LastGC.Format(time.Stamp), false),
		embedMakeField("Total garbage collections",
			gc.NumGC, false),
		embedMakeField("Total pauses for GC",
			gc.PauseTotal, false),
		embedMakeField("Average pause for GC",
			durationAverage(gc.Pause), false),
		embedMakeField("Gateway latency",
			gatewayLatency.String(), true),
	)

	// Get the ping
	start := time.Now()

	n, err := c.Ctx.SendEmbed(m.ChannelID, e)
	if err != nil {
		return errors.Wrap(err, "Failed to send the message")
	}

	duration := time.Now().Sub(start)

	e.Fields = append(e.Fields,
		embedMakeField("Message send latency", duration, true),
	)

	_, err = c.Ctx.EditEmbed(m.ChannelID, n.ID, e)
	if err != nil {
		return errors.Wrap(err, "Failed to edit the message")
	}

	runtime.GC()
	return nil
}

func embedMakeField(
	name string, value interface{}, inline bool) discord.EmbedField {
	return discord.EmbedField{
		Name:   name,
		Value:  fmt.Sprintf("%v", value),
		Inline: inline,
	}
}

func durationAverage(durations []time.Duration) time.Duration {
	var duration time.Duration

	for _, d := range durations {
		duration += d
	}

	duration /= time.Duration(len(durations))

	return duration
}
