package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/session"
	"github.com/diamondburned/arikawa/v2/state"
	"github.com/diamondburned/arikawa/v2/state/store"
	"gitlab.com/diamondburned/nixie/db"
)

func TestMain(t *testing.T) {
	var testDB = filepath.Join(os.TempDir(), "test.db")
	defer os.RemoveAll(testDB)

	db, err := db.NewKVFile(testDB)
	if err != nil {
		t.Fatal("failed to create the tags database:", err)
	}
	defer db.Close()

	cabinet := store.NoopCabinet
	cabinet.MeStore = meStore{}

	state := &state.State{
		Cabinet: cabinet,
		Session: &session.Session{
			Gateway: gateway.NewCustomGateway("", ""),
		},
	}

	c, err := bot.New(state, &Commands{})
	if err != nil {
		t.Fatal("failed to create bot:", err)
	}

	mainCtx(c, db, db)
}

type meStore struct {
	store.NoopStore
}

func (s meStore) Me() (*discord.User, error) {
	return &discord.User{
		ID:       1,
		Username: "nixie",
	}, nil
}
