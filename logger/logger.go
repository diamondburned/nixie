package logger

import (
	"fmt"
	"runtime/debug"
	"sync"
	"time"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/bot"
	args "github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
)

// map[*bot.Context]*Logger
var loggers sync.Map

type Logger struct {
	Ctx  *bot.Context
	node db.Node
}

type Settings struct {
	LogChannel discord.ChannelID
}

func Init(node db.Node) *Logger {
	return &Logger{
		node: node,
	}
}

func (l *Logger) Setup(sub *bot.Subcommand) {
	sub.Hidden = true
	sub.AddMiddleware("*", middlewares.GuildOnly(l.Ctx))
	sub.AddMiddleware("*", middlewares.AdminOnly(l.Ctx))

	// Register the context.
	loggers.Store(l.Ctx, l)
}

func (l *Logger) Test(m *gateway.MessageCreateEvent) (string, error) {
	iv, ok := loggers.Load(l.Ctx)
	if !ok {
		return "", errors.New("bot context is not registered (bug)")
	}

	logger := iv.(*Logger)

	var s Settings

	if err := logger.node.Get(m.GuildID.String(), &s); err != nil {
		return "", errors.New("your guild is not found")
	}

	_, err := l.Ctx.SendMessageComplex(s.LogChannel, api.SendMessageData{
		Content:         "Successfully tested the logger.",
		AllowedMentions: mentions,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to send the test message")
	}

	return fmt.Sprintf("Successfully sent to %s.", m.ChannelID.Mention()), nil
}

func (l *Logger) Activate(m *gateway.MessageCreateEvent, target args.ChannelMention) (string, error) {
	t, err := l.Ctx.Channel(discord.ChannelID(target))
	if err != nil {
		return "", errors.Wrap(err, "Failed to get mentioned channel")
	}

	// Try sending.
	_, err = l.Ctx.SendMessage(t.ID, "", &discord.Embed{
		Description: "Test log",
		Timestamp:   discord.Timestamp(time.Now()),
	})
	if err != nil {
		return "", errors.Wrap(err, "Failed to send a test message")
	}

	var s = Settings{
		LogChannel: t.ID,
	}

	if err := l.node.Set(m.GuildID.String(), s); err != nil {
		return "", errors.Wrap(err, "Failed to save settings")
	}

	return "Updated the logging channel to <#" + t.ID.String() + ">", nil
}

func Error(ctx *bot.Context, guildID discord.GuildID, err error) {
	if err == nil {
		return
	}

	var stack = string(debug.Stack())
	// Trim the stack.
	if len(stack) > 512 {
		stack = stack[:512-3] + "..."
	}

	Log(ctx, guildID, err.Error(), &discord.Embed{
		Color:       0xFF0000,
		Title:       "Stack Trace",
		Description: "```\n" + stack + "\n```",
		Timestamp:   discord.Timestamp(time.Now()),
	})
}

var mentions = &api.AllowedMentions{
	// Mention no-one.
	Parse: []api.AllowedMentionType{},
}

func Log(ctx *bot.Context, guildID discord.GuildID, c string, e *discord.Embed) {
	iv, ok := loggers.Load(ctx)
	if !ok {
		return
	}

	logger := iv.(*Logger)

	var s Settings

	if err := logger.node.Get(guildID.String(), &s); err != nil {
		return
	}

	send := api.SendMessageData{
		Content:         c,
		Embed:           e,
		AllowedMentions: mentions,
	}

	go logger.Ctx.SendMessageComplex(s.LogChannel, send)
}
