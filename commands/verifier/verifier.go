package verifier

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"github.com/tkuchiki/parsetime"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
	"gitlab.com/diamondburned/nixie/logger"
)

var timeParser = parsetime.NewParseTime()

type Verify struct {
	Ctx   *bot.Context
	users *Actives
	node  db.Node
}

type Settings struct {
	// Role to be given to the user.
	RoleID discord.RoleID

	Safezone time.Time
	Paused   bool

	// Information about the message that the reaction is on.
	ChannelID discord.ChannelID
	MessageID discord.MessageID
	AuthorID  discord.UserID
	// TODO: make this discord.APIEmoji
	Emoji string // (discord.Emoji).APIString()
}

func (s *Settings) fmtError(err error, msg string, user discord.User) error {
	return errors.Wrap(err, fmt.Sprintf(
		"<@%s>: %s to %s (%s)",
		s.AuthorID.String(), msg, user.Username, user.ID.String(),
	))
}

func New(node db.Node) *Verify {
	return &Verify{
		users: NewActives(),
		node:  node,
	}
}

const GreetMessage = "To complete the verification process, please answer the following question."

func (c *Verify) Setup(sub *bot.Subcommand) {
	dctools.ApplyMiddlewares(
		sub,
		middlewares.AdminOnly(c.Ctx),
		c.Pause,
		c.Initialize,
		c.React,
		c.Safezone,
		c.Disable,
	)
	sub.Hidden = true
	sub.FindCommand(c.Cancel).Description = "Cancel the current verification."
	sub.FindCommand(c.Pause).Description = "Skip asking questions."
	sub.FindCommand(c.Safezone).Arguments[0].String = "[date]"
}

// Cancel allows some members to get a new question.
func (c *Verify) Cancel(m *gateway.MessageCreateEvent) (string, error) {
	if ok := c.users.TryCancel(m.GuildID, m.Author.ID); ok {
		return "Verification cancelled.", nil
	}

	return "", errors.New("No active verification found in this guild to cancel.")
}

func (c *Verify) ReactionAdd(r *gateway.MessageReactionAddEvent) {
	// Ignore bots.
	if r.Member != nil && r.Member.User.Bot {
		return
	}

	var s *Settings
	if err := c.node.Get(r.GuildID.String(), &s); err != nil {
		// unknown guild
		return
	}

	// Check if this is the right message:
	if s.ChannelID != r.ChannelID || s.MessageID != r.MessageID {
		return
	}
	// Check if this is the right reaction:
	if discord.APIEmoji(s.Emoji) != r.Emoji.APIString() {
		return
	}
	// Ignore owner.
	if r.UserID == s.AuthorID {
		return
	}

	defer func() {
		// Remove the reaction at the end:
		err := c.Ctx.DeleteUserReaction(
			r.ChannelID, r.MessageID, r.UserID, discord.APIEmoji(s.Emoji))
		if err != nil {
			logger.Error(c.Ctx, r.GuildID, errors.Wrap(err, "Failed to unreact"))
		}
	}()

	// Check if the current user is already being verified.
	ctx, ok := c.users.Take(r.GuildID, r.UserID)
	if !ok {
		return
	}

	// Free the user afterwards.
	defer c.users.Done(r.UserID)

	if r.Member == nil {
		m, err := c.Ctx.Member(r.GuildID, r.UserID)
		if err != nil {
			logger.Error(c.Ctx, r.GuildID, errors.Wrap(err, "Failed to get member"))
			return
		}
		r.Member = m
	}

	// Check if the member already has the role:
	for _, id := range r.Member.RoleIDs {
		if id == s.RoleID {
			// Member already has role, skip.
			return
		}
	}

	// Check if user joined before date:
	if s.Paused || before(r.Member.Joined.Time(), s.Safezone) {
		// Give them the role.
		if err := c.Ctx.AddRole(r.GuildID, r.UserID, s.RoleID); err != nil {
			// Can't do much if things fail, so we ping the owner.
			err = s.fmtError(err, "Failed to add role", r.Member.User)
			logger.Error(c.Ctx, r.GuildID, err)
		}
		return
	}

	// prompt
	if err := c.prompt(ctx, r.Member, r.GuildID, s); err != nil {
		logger.Error(c.Ctx, r.GuildID, err)
	}
}

func (c *Verify) prompt(
	ctx context.Context, m *discord.Member, guildID discord.GuildID, s *Settings) error {

	// Get the DM channel.
	dm, err := c.Ctx.CreatePrivateChannel(m.User.ID)
	if err != nil {
		// Silently log this.
		return s.fmtError(err, "Failed to make a DM channel", m.User)
	}

	var question = RandomQuestion()
	var message = GreetMessage + "\n\n" + question.Question

	// Send the greet message over to the DM channel.
	_, err = c.Ctx.SendMessage(dm.ID, message, nil)
	if err != nil {
		return s.fmtError(err, "Failed to send messages", m.User)
	}

	// Listen for the message using the given context with a 7min timeout:
	ctx, cancel := context.WithTimeout(ctx, 7*time.Minute)
	defer cancel()

	v := c.Ctx.WaitFor(ctx, func(v interface{}) bool {
		ms, ok := v.(*gateway.MessageCreateEvent)
		if !ok {
			return false
		}
		return ms.ChannelID == dm.ID && ms.Author.ID == m.User.ID
	})

	if v == nil {
		// Non-important error.
		c.Ctx.SendMessage(dm.ID, "Timed out waiting for an answer.", nil)
		return nil
	}

	// Verify the answer.
	msg := v.(*gateway.MessageCreateEvent)
	if !question.Answer(strings.ToLower(msg.Content)) {
		c.Ctx.SendMessage(dm.ID, "Incorrect answer, please re-react and try again.", nil)
		return nil
	}

	c.Ctx.SendMessage(dm.ID, "Correct answer!", nil)

	// Answer is correct. Give them the role.
	if err := c.Ctx.AddRole(guildID, m.User.ID, s.RoleID); err != nil {
		// Pretty fatal error, so we'll warn the user too.
		c.Ctx.SendMessage(dm.ID, "Unfortunately, we couldn't give you the role."+
			"Please contact one of the administrators.", nil)

		return s.fmtError(err, "Failed to add role", m.User)
	}

	return nil
}

func (c *Verify) Initialize(
	e *gateway.MessageCreateEvent,
	url arguments.MessageURL, roleString dctools.Role, emoji arguments.Emoji) (string, error) {

	role := roleString.Search(c.Ctx, e.GuildID)
	if role == nil {
		return "", errors.New("failed to find role.")
	}

	if emoji.Custom {
		// Confirm the emoji exists:
		_, err := c.Ctx.Emoji(e.GuildID, emoji.ID)
		if err != nil {
			return "", errors.Wrap(err, "failed to find emoji")
		}
	}

	// Get the message:
	m, err := c.Ctx.Message(url.ChannelID, url.MessageID)
	if err != nil {
		return "", errors.Wrap(err, "failed to get linked message")
	}

	// Get the channel:
	ch, err := c.Ctx.Channel(url.ChannelID)
	if err != nil {
		return "", errors.Wrap(err, "failed to get linked channel")
	}

	// Verify that it's in the same guild:
	if ch.GuildID != e.GuildID {
		return "", errors.New("linked message is not in the same guild.")
	}

	var s = Settings{
		RoleID:    role.ID,
		ChannelID: m.ChannelID,
		MessageID: m.ID,
		AuthorID:  m.Author.ID,
		Emoji:     emoji.APIString(),
	}

	// React to the message.
	if err := c.Ctx.React(s.ChannelID, s.MessageID, discord.APIEmoji(s.Emoji)); err != nil {
		return "", errors.Wrap(err, "failed to react to message")
	}

	// Save the settings after reacting.
	if err := c.node.Set(e.GuildID.String(), s); err != nil {
		return "", errors.Wrap(err, "failed to save settings")
	}

	return "Done.", nil
}

func (c *Verify) React(m *gateway.MessageCreateEvent) (string, error) {
	var s *Settings
	if err := c.node.Get(m.GuildID.String(), &s); err != nil {
		return "", errors.New("guild is not enabled.")
	}

	// React to the message.
	if err := c.Ctx.React(s.ChannelID, s.MessageID, discord.APIEmoji(s.Emoji)); err != nil {
		return "", errors.Wrap(err, "failed to react to message")
	}

	return "Reacted.", nil
}

func (c *Verify) Safezone(e *gateway.MessageCreateEvent, raw bot.RawArguments) (string, error) {
	var s *Settings
	if err := c.node.Get(e.GuildID.String(), &s); err != nil {
		return "", errors.New("guild is not enabled.")
	}

	if string(raw) == "" {
		if s.Safezone.IsZero() {
			return "No safezone.", nil
		}
		return "Safezone: " + s.Safezone.String() + ".", nil
	}

	// Not empty, parse (UTC by default):
	t, err := timeParser.Parse(string(raw))
	if err != nil {
		return "", errors.Wrap(err, "invalid time")
	}

	s.Safezone = t
	if err := c.node.Set(e.GuildID.String(), s); err != nil {
		return "", errors.Wrap(err, "failed to save settings")
	}

	return "Safezone set to " + s.Safezone.String() + ".", nil
}

func (c *Verify) Pause(e *gateway.MessageCreateEvent, pause bool) (string, error) {
	var s *Settings
	if err := c.node.Get(e.GuildID.String(), &s); err != nil {
		// unknown guild
		return "", errors.New("guild is not enabled")
	}

	s.Paused = pause

	if err := c.node.Set(e.GuildID.String(), s); err != nil {
		return "", errors.Wrap(err, "failed to save settings")
	}

	if !pause {
		return "Verifier resumed. Questions will be direct messaged.", nil
	}

	return "Paused verifier. Any reaction will automatically give the role.", nil
}

func (c *Verify) Disable(e *gateway.MessageCreateEvent) (string, error) {
	var s *Settings
	if err := c.node.Get(e.GuildID.String(), &s); err != nil {
		return "", errors.New("guild is not enabled")
	}

	if err := c.node.Delete(e.GuildID.String()); err != nil {
		return "", errors.Wrap(err, "failed to delete")
	}

	// Finalize.
	c.Ctx.Unreact(s.ChannelID, s.MessageID, discord.APIEmoji(s.Emoji))

	return "Disabled.", nil
}

func before(this, isBefore time.Time) bool {
	if this.IsZero() || isBefore.IsZero() {
		return false
	}
	return this.Before(isBefore)
}
