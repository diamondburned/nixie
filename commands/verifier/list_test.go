package verifier

import (
	"strconv"
	"testing"
	"time"
)

func TestQuestions(t *testing.T) {
	t.Run("date", func(t *testing.T) {
		q := Questions["date"]

		// Emulate the command.
		h := time.Now().UTC().Hour()

		if !q.Answer(strconv.Itoa(h)) {
			t.Fatal("Wrong answer.")
		}
	})

	t.Run("git", func(t *testing.T) {
		q := Questions["git"]
		if !q.Answer("git push") {
			t.Fatal("Wrong answer.")
		}
	})

	t.Run("ed", func(t *testing.T) {
		if !Questions["ed"].Answer("ed") {
			t.Fatal("Wrong answer.")
		}
	})

	t.Run("cd", func(t *testing.T) {
		if !Questions["cd"].Answer("cd") {
			t.Fatal("Wrong answer.")
		}
	})

	t.Run("math", func(t *testing.T) {
		if !Questions["math"].Answer("30") {
			t.Fatal("Wrong answer.")
		}
	})
}
