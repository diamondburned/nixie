package verifier

import (
	"context"
	"sync"

	"github.com/diamondburned/arikawa/v2/discord"
)

// seat represents an active seat where the user is being verified.
type seat struct {
	cancel func()
	guild  discord.GuildID
}

type Actives struct {
	mutex sync.RWMutex
	users map[discord.UserID]seat
}

func NewActives() *Actives {
	return &Actives{
		users: map[discord.UserID]seat{},
	}
}

func (a *Actives) Take(guild discord.GuildID, user discord.UserID) (context.Context, bool) {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	if _, ok := a.users[user]; ok {
		return nil, false
	}

	ctx, cancel := context.WithCancel(context.Background())

	a.users[user] = seat{
		cancel: cancel,
		guild:  guild,
	}

	return ctx, true
}

// TryCancel tries to remove the user off the map. This only works if the user
// is doing this in the same guild as the one they're being verified on.
func (a *Actives) TryCancel(guild discord.GuildID, user discord.UserID) (allow bool) {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	if seat, ok := a.users[user]; ok {
		if seat.guild == guild {
			seat.cancel()
			delete(a.users, user)
			return true
		}
	}

	return false
}

func (a *Actives) Done(user discord.UserID) {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	if seat, ok := a.users[user]; ok {
		seat.cancel()
		delete(a.users, user)
	}
}
