package verifier

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Question struct {
	Question string
	Answer   func(input string) bool
}

var Questions = map[string]Question{
	"date": {
		// The question asks for the hour in UTC.
		Question: "What's the output of `date -u +%H`?",
		Answer: func(input string) bool {
			// The output will be 15 (the hour in UTC).

			h, err := strconv.Atoi(input)
			if err != nil {
				return false
			}

			// get current time
			now := time.Now().UTC()

			// Allow a small margin of error (1 hour):
			if abs(h-now.Hour()) <= 1 {
				return true
			}
			return false
		},
	},
	"git": {
		Question: "What's the command to push to the remote repository in Git?",
		Answer: func(input string) bool {
			// accept anything, such as git push remote origin
			return strings.HasPrefix(input, "git push")
		},
	},
	"ed": {
		Question: "What is the standard text editor?",
		Answer: func(input string) bool {
			return strings.HasPrefix(input, "ed")
		},
	},
	"cd": {
		Question: "What's the command to change directories?",
		Answer: func(input string) bool {
			return strings.HasPrefix(input, "cd")
		},
	},
	"math": {
		Question: "What's `5 * (3+3)`?",
		Answer: func(input string) bool {
			return input == "30"
		},
	},
}

func RandomQuestion() (q Question) {
	i, r := 0, rand.Intn(len(Questions))
	for _, question := range Questions {
		if i == r {
			q = question
			break
		}
		i++
	}
	return
}

func abs(i int) int {
	if i < 0 {
		return i * -1
	}
	return i
}
