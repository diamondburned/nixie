package rextester

import (
	"encoding/json"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/commands/playground"
	"gitlab.com/diamondburned/nixie/commands/playground/client"
)

const (
	// Compile is the constant to the compile URL
	Compile = "https://rextester.com/rundotnet/Run"
)

const CSharpShorthand = `
%s

namespace Rextester {
	public class Program {
		public static void Main(string[] args) {
			%s
		}
	}
}
`

const CShorthand = `
%s

int main(int argc, char** argv) {
	%s
}
`

const KotlinShorthand = `
%s

fun main(args: Array<String>) {
	%s
}
`

const AssemblyShorthand = `
section .data
	%s

section .text
	%s
`

var LanguageChoices = map[string]LanguageChoice{
	"sh":   {"38", "%s\n%s", ""},
	"bash": {"38", "%s\n%s", ""},

	"r":      {"31", "%s\n%s", ""},
	"py":     {"24", "%s\n%s", ""},
	"python": {"24", "%s\n%s", ""},

	"js":         {"23", "%s\n%s", ""}, // NodeJS over Rhino
	"javascript": {"23", "%s\n%s", ""},

	"lua": {"14", "%s\n%s", ""},

	"swift": {"37", "%s\n%s", ""},

	"cs": {"1", CSharpShorthand, ""},

	"c":   {"26", CShorthand, "-Wall -std=c17 -O3 -o a.out source_file.c"},
	"cpp": {"27", CShorthand, "-Wall -std=c++2a -stdlib=libc++ -O3 -o a.out source_file.cpp"},

	"kt":     {"43", KotlinShorthand, ""},
	"kotlin": {"43", KotlinShorthand, ""},

	"asm":      {"15", AssemblyShorthand, ""},
	"assembly": {"15", AssemblyShorthand, ""},

	"hs":      {"11", "%s\n%s", ""},
	"haskell": {"11", "%s\n%s", ""},
}

func init() {
	for name, choice := range LanguageChoices {
		playground.Register(name, choice)
	}
}

// LanguageChoice is a language choice for Rextester. The WrapperID represents
// the arbitrary ID that would be used in the LanguageChoiceWrapper parameter.
type LanguageChoice struct {
	WrapperID    string
	Shorthandf   string
	CompilerArgs string
}

func (choice LanguageChoice) Shorthand() string {
	return playground.ReplaceShorthandf(choice.Shorthandf)
}

func (choice LanguageChoice) ExecuteShorthand(header, code string) (*playground.Result, error) {
	return choice.ExecuteFull(fmt.Sprintf(choice.Shorthandf, header, code))
}

var statsRegex = regexp.MustCompile(`absolute service time: (\d+,?\d+) sec`)

func (choice LanguageChoice) ExecuteFull(code string) (*playground.Result, error) {
	req, err := client.Post(Compile, url.Values{
		"LanguageChoiceWrapper": {choice.WrapperID},
		"EditorChoiceWrapper":   {"3"},
		"LayoutChoiceWrapper":   {"1"},
		"Program":               {code},
		"CompilerArgs":          {choice.CompilerArgs},
		"Input":                 {""},
		"Privacy":               {""},
		"PrivacyUsers":          {""},
		"Title":                 {""},
		"SavedOutput":           {""},
		"WholeError":            {""},
		"WholeWarning":          {""},
		"StatsToSave":           {""},
		"CodeGuid":              {""},
		"IsInEditMode":          {"False"},
		"IsLive":                {"False"},
	})

	if err != nil {
		return nil, errors.Wrap(err, "Server error")
	}

	defer req.Body.Close()

	var data struct {
		Errors   string
		Files    string
		Result   string
		Stats    string
		Warnings string
	}

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		return nil, errors.Wrap(err, "Failed to decode JSON")
	}

	var dura time.Duration
	if matches := statsRegex.FindStringSubmatch(data.Stats); matches != nil {
		secs, _ := strconv.ParseFloat(strings.ReplaceAll(matches[1], ",", "."), 64)
		dura = time.Duration(secs * float64(time.Second)).Round(time.Millisecond)
	}

	return &playground.Result{
		Errors: strings.TrimSpace(data.Warnings + "\n" + data.Errors),
		Output: data.Result,
		Dura:   dura,
	}, nil
}
