package golang

import (
	"strings"
	"testing"
)

func TestGolang(t *testing.T) {
	code := `fmt.Println("Renzix gay")`

	e := Golang{}

	r, err := e.ExecuteShorthand("", code)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(r.Output, "Renzix gay") {
		t.Fatal("Got unexpected output:", r.Output)
	}

	// Execute code with an error:
	r, err = e.ExecuteShorthand("", code[7:])
	if err != nil {
		t.Fatal(err)
	}

	if r.Errors == "" {
		t.Fatal("Empty error:", r)
	}
}
