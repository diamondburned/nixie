package golang

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/commands/playground"
	"gitlab.com/diamondburned/nixie/commands/playground/client"
)

const (
	// Compile is the constant to the compile URL
	Compile = "https://play.golang.org/compile"

	// Fmt is the constant to the gofmt/goimports URL
	Fmt = "https://play.golang.org/fmt"
)

func init() {
	playground.Register("go", Golang{})
	playground.Register("golang", Golang{})
}

const GoShorthand = `
package main

// goimports to be automatically executed.
%s

func main() {
	%s
}
`

type Golang struct{}

var _ playground.Executer = (*Golang)(nil)

func (g Golang) Shorthand() string {
	return playground.ReplaceShorthandf(GoShorthand)
}

func (g Golang) ExecuteShorthand(
	header, code string) (*playground.Result, error) {

	return g.ExecuteFull(fmt.Sprintf(GoShorthand, header, code))
}

func (g Golang) ExecuteFull(code string) (*playground.Result, error) {
	c, err := g.goimports(code)
	if err != nil {
		return nil, err
	}

	req, err := client.Post(Compile, url.Values{
		"version": {"2"},
		"body":    {c},
	})

	if err != nil {
		return nil, errors.Wrap(err, "Failed to execute code")
	}

	defer req.Body.Close()

	var reply struct {
		Errors string `json:"Errors"`
		Events []struct {
			Message string `json:"Message"`
			Kind    string `json:"Kind"`
			Delay   int64  `json:"Delay"`
		} `json:"Events"`

		Status      int64 `json:"Status"`
		IsTest      bool  `json:"IsTest"`
		TestsFailed int64 `json:"TestsFailed"`
	}

	if err := json.NewDecoder(req.Body).Decode(&reply); err != nil {
		return nil, errors.Wrap(err, "Server error")
	}

	var result = &playground.Result{
		Errors: reply.Errors,
	}
	for _, ev := range reply.Events {
		result.Output += ev.Message
		result.Dura += time.Duration(ev.Delay)
	}

	return result, nil
}

func (g Golang) goimports(input string) (string, error) {
	req, err := client.Post(Fmt, url.Values{
		"body":    {input},
		"imports": {"true"},
	})

	if err != nil {
		return "", err
	}

	defer req.Body.Close()

	var imports struct {
		Body  string
		Error string
	}

	if err := json.NewDecoder(req.Body).Decode(&imports); err != nil {
		return "", errors.Wrap(err, "Failed to run goimports")
	}

	if imports.Error != "" {
		return "", errors.New(imports.Error)
	}

	return imports.Body, nil
}
