package playground

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/utils/sendpart"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

var playgrounds = map[string]Executer{}

func Register(name string, pg Executer) {
	playgrounds[name] = pg
}

type Executer interface {
	Shorthand() string
	ExecuteFull(code string) (*Result, error)
	ExecuteShorthand(header, code string) (*Result, error)
}

func ReplaceShorthandf(shorthandf string) string {
	shorthandf = strings.Replace(shorthandf, "%s", "{header}", 1)
	shorthandf = strings.Replace(shorthandf, "%s", "{code}", 1)
	return shorthandf
}

type Result struct {
	Output string
	Errors string
	Image  []byte
	Dura   time.Duration
}

const usage = "[\"raw\"] language code"

var re = regexp.MustCompile(`(?ms)^\x60\x60\x60(\S+)\n(.*)\x60\x60\x60`)

type Playground struct {
	Ctx *bot.Context
}

func New() *Playground { return &Playground{} }

func lines(lines ...string) string {
	return strings.Join(lines, "\n")
}

func (p *Playground) Help() string {
	builder := strings.Builder{}
	builder.WriteString("playground list: " +
		"print supported languages\n")
	builder.WriteString("playground shorthand __language__: " +
		"print the language's shorthand template\n")

	builder.WriteString("Examples:\n")
	builder.WriteString(
		"	playground go fmt.Println(\"code\")")
	builder.WriteString(lines(
		"	playground \\`\\`\\`go",
		`		:import "fmt"`,
		`		fmt.Println("code")`,
		"	\\`\\`\\`\n"))
	builder.WriteString(lines(
		"	playground raw \\`\\`\\`go",
		`		package main`,
		`		import "fmt"`,
		`		func main() { fmt.Println("code") }`,
		"	\\`\\`\\`"))

	return builder.String()
}

func (p *Playground) Setup(sub *bot.Subcommand) {
	sub.Description = "Evaluate code online."
	sub.SetPlumb(p.Run)
}

func (p *Playground) List(m *gateway.MessageCreateEvent) (string, error) {
	var languages = make([]string, 0, len(playgrounds))
	for name := range playgrounds {
		languages = append(languages, name)
	}

	sort.Strings(languages)

	return fmt.Sprintf("Supported languages: %s.", strings.Join(languages, ", ")), nil
}

func (p *Playground) Shorthand(m *gateway.MessageCreateEvent, lang string) (string, error) {
	pg, ok := playgrounds[lang]
	if !ok {
		return "", errors.New("Unknown language: " + lang)
	}

	return fmt.Sprintf("```%s\n%s\n```", lang, pg.Shorthand()), nil
}

func (p *Playground) Run(
	m *gateway.MessageCreateEvent, args bot.RawArguments) (*api.SendMessageData, error) {

	var raw = false
	if strings.HasPrefix(string(args), "raw ") {
		raw = true
		args = args[len("raw "):]
	}

	matches := re.FindStringSubmatch(string(args))

	var code string
	var lang string

	// If code block is not found:
	if len(matches) != 3 {
		parts := strings.SplitN(string(args), " ", 2)
		if len(parts) != 2 {
			return nil, errors.New("Invalid usage: `playground " + usage + "`")
		}

		code = parts[1]
		lang = parts[0]
	} else {
		code = matches[2]
		lang = matches[1]
	}

	pg, ok := playgrounds[lang]
	if !ok {
		return nil, errors.New("Unknown language: " + lang)
	}

	var res *Result
	var err error

	if raw {
		res, err = pg.ExecuteFull(code)
	} else {
		var lines = strings.Split(code, "\n")
		var head, body string

		for _, l := range lines {
			l = strings.TrimSpace(l)

			if len(l) > 0 && l[0] == ':' {
				head += l[1:] + "\n"
			} else {
				body += l + "\n"
			}
		}

		res, err = pg.ExecuteShorthand(head, body)
	}

	if err != nil {
		return nil, err
	}

	s := api.SendMessageData{
		Embed: &discord.Embed{
			Author: &discord.EmbedAuthor{
				Name: p.Ctx.AuthorDisplayName(m),
				Icon: m.Author.AvatarURL(),
			},
			Timestamp: discord.NowTimestamp(),
		},
	}

	if res.Dura > 0 {
		s.Embed.Footer = &discord.EmbedFooter{
			Text: res.Dura.String(),
		}
	}

	switch {
	case res.Errors != "":
		s.Embed.Color = 0xFF0000
		s.Embed.Description = dctools.WrapCodeBlock(res.Errors, "")

	case res.Image != nil:
		s.Files = []sendpart.File{{
			Name:   "image.png",
			Reader: bytes.NewReader(res.Image),
		}}

		s.Embed.Image = &discord.EmbedImage{
			URL: "attachment://image.png",
		}

	case res.Errors == "" && res.Output == "":
		s.Embed.Description = "<empty>"

	default:
		s.Embed.Color = 0x00FF00
		s.Embed.Description = dctools.WrapCodeBlock(res.Output, "")
	}

	return &s, nil
}
