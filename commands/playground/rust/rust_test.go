package rust

import (
	"strings"
	"testing"
)

func TestRust(t *testing.T) {
	code := `println!("Renzix gay")`

	e := Rustlang{}

	r, err := e.ExecuteShorthand("", code)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(r.Output, "Renzix gay") {
		t.Fatal("Got unexpected output: " + r.Output)
	}

	// Execute code with an error:
	r, err = e.ExecuteShorthand("", code[1:])
	if err != nil {
		t.Fatal(err)
	}

	if r.Errors == "" {
		t.Fatal("Empty error:", r)
	}
}
