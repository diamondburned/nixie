package rust

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/commands/playground"
	"gitlab.com/diamondburned/nixie/commands/playground/client"
)

const (
	// Compile is the constant to the compile URL
	Compile = "https://play.rust-lang.org/execute"

	RustChannel = "stable"
	RustEdition = "2018"
	RustMode    = "debug"
	RustCrateT  = "bin"
)

func init() {
	playground.Register("rs", Rustlang{})
	playground.Register("rust", Rustlang{})
}

type rustDefaults struct {
	Channel   string `json:"channel"`
	Mode      string `json:"mode"`
	Edition   string `json:"edition"`
	CrateType string `json:"crateType"`
	Tests     bool   `json:"tests"`
	Code      string `json:"code"`
	Backtrace bool   `json:"backtrace"`
}

const RustShorthand = `
%s

fn main() {
	%s
}
`

type Rustlang struct{}

var _ playground.Executer = (*Rustlang)(nil)

func (rs Rustlang) Shorthand() string {
	return playground.ReplaceShorthandf(RustShorthand)
}

func (rs Rustlang) ExecuteShorthand(
	header, code string) (*playground.Result, error) {

	return rs.ExecuteFull(fmt.Sprintf(RustShorthand, header, code))
}

func (rs Rustlang) ExecuteFull(code string) (*playground.Result, error) {
	r, err := client.Post(Compile, rustDefaults{
		Channel:   RustChannel,
		Mode:      RustMode,
		Edition:   RustEdition,
		CrateType: RustCrateT,
		Tests:     false,
		Code:      code,
		Backtrace: false,
	})

	if err != nil {
		return nil, errors.Wrap(err, "Server error")
	}

	defer r.Body.Close()

	var reply struct {
		Success bool   `json:"success"`
		Stdout  string `json:"stdout"`
		Stderr  string `json:"stderr"`
	}

	if err := json.NewDecoder(r.Body).Decode(&reply); err != nil {
		return nil, errors.Wrap(err, "Server error")
	}

	result := &playground.Result{
		Output: reply.Stdout,
	}

	if reply.Success {
		return result, nil
	}

	result.Errors = reply.Stderr
	return result, nil
}
