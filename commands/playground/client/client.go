package client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

// Client sets a custom timeout for playgrounds.
// Playgrounds are expected to use this client for HTTP requests.
var Client = &http.Client{
	Timeout: 10 * time.Second,
}

// Post sends a POST request with the client
func Post(u string, body interface{}) (*http.Response, error) {
	switch body := body.(type) {
	case io.Reader:
		return post(u, body)

	case url.Values:
		return http.PostForm(u, body)

	case nil:
		r, err := http.NewRequest("POST", u, nil)
		if err != nil {
			return nil, err
		}

		return Client.Do(r)

	default:
		j, err := json.Marshal(body)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to encode to JSON")
		}

		return post(u, bytes.NewReader(j))
	}
}

func post(u string, body io.Reader) (*http.Response, error) {
	r, err := http.NewRequest("POST", u, body)
	if err != nil {
		return nil, err
	}

	r.Header.Set("Content-Type", "application/json")

	return Client.Do(r)
}
