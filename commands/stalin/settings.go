package stalin

import (
	"bytes"
	"sort"
	"strings"
	"sync"

	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/logger"
)

// Settings contains kv store for settings
type Settings struct {
	BlacklistedWords  []string
	ExceptionChannels []discord.ChannelID
}

func (s Settings) maxWordLen() int {
	var length int
	for _, w := range s.BlacklistedWords {
		if len(w) > length {
			length = len(w)
		}
	}

	return length
}

func (s *Stalin) getSettings(guild discord.GuildID) (*Settings, error) {
	var settings *Settings
	return settings, s.node.Get(guild.String(), &settings)
}

// CheckDelete checks one or multiple messages for blacklisted words.
// Nothing is returned if the message does not have the word.
func (s *Stalin) checkDelete(m discord.Message) []discord.MessageID {
	if m.Author.Bot {
		return nil
	}

	// Check if the guild is in the database:
	settings, err := s.getSettings(m.GuildID)
	if err != nil {
		// Drop the event if it's not.
		if errors.Is(err, db.ErrKeyNotFound) {
			return nil
		}

		// Unknown error; we should log this.
		logger.Error(s.Ctx, m.GuildID, errors.Wrap(err, "Failed to get settings"))
		return nil
	}

	// Check if the channel is in one of the whitelists
	for _, g := range settings.ExceptionChannels {
		if g == m.ChannelID {
			return nil
		}
	}

	// Sanitize
	content := Filter(m.Content)

	// See if the message contains any blacklisted words
	for _, w := range settings.BlacklistedWords {
		if strings.Contains(content, w) {
			return []discord.MessageID{m.ID}
		}
	}

	return s.checkInStore(m, *settings)
}

type channel struct {
	sync.Mutex
	guild   discord.GuildID
	authors []*author
}

type author struct {
	id       discord.UserID
	messages []message
}

func (a author) last() message {
	return a.messages[len(a.messages)-1]
}

type message struct {
	id      discord.MessageID
	content string
}

func genMessage(m discord.Message) message {
	msg := message{id: m.ID, content: m.Content}

	for _, e := range m.Embeds {
		msg.content += e.Description
	}

	return msg
}

const maxAuthors = 5

func (s *Stalin) checkInStore(
	m discord.Message, settings Settings) []discord.MessageID {

	// Grab the message store
	v, _ := s.chs.LoadOrStore(m.ChannelID, &channel{
		guild: m.GuildID,
	})

	ch := v.(*channel)

	// Start locking that individual message store mutex
	ch.Lock()
	defer ch.Unlock()

	// Find the author:
	var au *author

	for _, a := range ch.authors {
		if a.id == m.Author.ID {
			au = a
			break
		}
	}

	// Can't find the author:
	if au == nil {
		// Add it into the slice with this message:
		au = &author{
			id:       m.Author.ID,
			messages: make([]message, 0, 1),
		}
		ch.authors = append(ch.authors, au)
	}

	// Sort so that the authors with the latest messages are at the end:
	sort.Slice(ch.authors, func(i, j int) bool {
		if len(ch.authors[i].messages) == 0 {
			// Put in front:
			return true
		}

		if len(ch.authors[i].messages) == 0 {
			// Put also in front:
			return false
		}

		idi := ch.authors[i].last().id
		idj := ch.authors[j].last().id

		return idi < idj
	})

	// Clean up the authors:
	if len(ch.authors) > maxAuthors {
		ch.authors = ch.authors[len(ch.authors)-maxAuthors:]
	}

	// Add our current message into the author:
	au.messages = append(au.messages, genMessage(m))

	// Trim the messages:
	if max := len(settings.BlacklistedWords); len(au.messages) > max {
		au.messages = au.messages[len(au.messages)-max:]
	}

	for _, w := range settings.BlacklistedWords {
		ids := wordInSlice(au.messages, w)
		if len(ids) > 0 {
			au.messages = au.messages[:len(au.messages)-len(ids)]
			return ids
		}
	}

	return nil
}

func wordInSlice(sl []message, word string) []discord.MessageID {
	var s bytes.Buffer
	var w = []byte(word)

	for i := len(sl) - 1; i >= 0; i-- {
		// Prepend the last message into the buffer
		p := s.String()
		s.Truncate(0)
		s.WriteString(sl[i].content)
		s.WriteString(p)

		// Check for blacklisted word
		if bytes.HasPrefix(s.Bytes(), w) {
			// Append IDs to be deleted into the slice
			ms := sl[i:]

			var ids = make([]discord.MessageID, len(ms))
			for j, m := range ms {
				ids[j] = m.id
			}

			return ids
		}
	}

	return nil
}
