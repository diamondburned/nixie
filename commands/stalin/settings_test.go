package stalin

import (
	"testing"

	"github.com/diamondburned/arikawa/v2/discord"
)

type sample struct {
	blacklist string
	messages  []string
	expected  []discord.MessageID
}

func TestWordInSlice(t *testing.T) {
	var samples = [...]sample{
		{"slice", []string{"this", "is", "a", "sl", "ice"},
			[]discord.MessageID{3, 4}},
		{"slice", []string{"this", "is", "random"},
			nil},
		{"cringe", []string{"cring\u0435"}, // Cyrillic e
			[]discord.MessageID{0}},
		{"cringe", []string{"crin\u200bge"},
			[]discord.MessageID{0}}, // 200B no-width space
	}

	var w []discord.MessageID

	for i, sample := range samples {
		w = wordInSlice(
			testGenerateMessages(sample.messages), sample.blacklist)

		cmp(t, sample, w, i+1)
	}
}

func testGenerateMessages(msgs []string) []message {
	mg := make([]message, len(msgs))
	for i, m := range msgs {
		// Filter is at delete.go:CheckDelete()
		m = Filter(m)

		mg[i] = message{
			id: discord.MessageID(i), content: m,
		}
	}

	return mg
}

func testEqualArray(a1, a2 []discord.MessageID) bool {
	if len(a1) != len(a2) {
		return false
	}

	for i := 0; i < len(a1); i++ {
		if a1[i] != a2[i] {
			return false
		}
	}

	return true
}

func cmp(t *testing.T, s sample, result []discord.MessageID, i int) {
	if len(s.expected) == 0 && len(result) == 0 {
		return
	}

	if testEqualArray(s.expected, result) {
		return
	}

	t.Fatalf(`Index: %d
		Test failed on word %s
		Expected %+v
		Received %+v`,

		i, s.blacklist,
		s.expected,
		result,
	)
}
