package stalin

import (
	"fmt"
	"strings"
	"sync"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

type Stalin struct {
	Ctx  *bot.Context
	node db.Node

	chs sync.Map // snowflake : *channel
}

func New(node db.Node) *Stalin {
	return &Stalin{
		node: node,
	}
}

func (s *Stalin) Setup(sub *bot.Subcommand) {
	sub.Description = "Always watching."
	sub.Hidden = true
	sub.AddMiddleware("*", middlewares.GuildOnly(s.Ctx))
	sub.AddMiddleware("*", s.createMiddleware)
	dctools.ApplyMiddlewares(sub, middlewares.AdminOnly(s.Ctx),
		s.Except, s.List, s.Censor, s.Reset,
	)

	sub.FindCommand(s.Censor).Arguments[0].String = "word"
	sub.FindCommand(s.Except).Arguments[0].String = "[#channel]"

	sub.ChangeCommandInfo(s.Reset, "", "Delete all words.")
	sub.ChangeCommandInfo(s.List, "", "List all censored words.")
	sub.ChangeCommandInfo(s.Censor, "", "Censor a word.")
	sub.ChangeCommandInfo(s.Except, "", "Add an exception channel.")
}

// createMiddleware handles all incoming messages and delete them if
// blacklisted words are found.
func (s *Stalin) createMiddleware(m *gateway.MessageCreateEvent) error {
	return s.handler(m.Message)
}

// HandlerEdit wraps around MessageEdit events
func (s *Stalin) HandlerEdit(e *gateway.MessageUpdateEvent) error {
	// Update messages do not have author, working around that:
	m, err := s.Ctx.Message(e.ChannelID, e.ID)
	if err != nil {
		return nil
	}

	return s.handler(discord.Message(*m))
}

func (s *Stalin) handler(m discord.Message) error {
	switch IDs := s.checkDelete(m); len(IDs) {
	case 0:
		return nil
	case 1:
		return s.Ctx.DeleteMessage(m.ChannelID, IDs[0])
	default:
		return s.Ctx.DeleteMessages(m.ChannelID, IDs)
	}
}

var vowels = [256]bool{
	'a': true,
	'i': true,
	'u': true,
	'e': true,
	'o': true,
}

func censorVowels(word string) string {
	censored := strings.Builder{}
	censored.Grow(len(word) * 2)

	for _, letter := range []byte(word) {
		if !vowels[letter] {
			censored.WriteByte(letter)
			continue
		}

		censored.WriteByte('\\')
		censored.WriteByte('*')
	}

	return censored.String()
}

func (s *Stalin) Reset(m *gateway.MessageCreateEvent) (string, error) {
	if err := s.node.Delete(m.GuildID.String()); err != nil {
		return "", errors.Wrap(err, "Failed to reset")
	}
	return "Done.", nil
}

type listFlags struct {
	fs *arguments.FlagSet

	Uncensor bool
}

var (
	_ bot.Usager       = (*listFlags)(nil)
	_ bot.ManualParser = (*listFlags)(nil)
)

func (args *listFlags) ensureFlags() {
	if args.fs == nil {
		args.fs = arguments.NewFlagSet()
		args.fs.BoolVar(&args.Uncensor, "uncensor", false, "uncensor words")
	}
}

func (args *listFlags) ParseContent(v []string) error {
	args.ensureFlags()
	return args.fs.Parse(v)
}

func (args *listFlags) Usage() string {
	args.ensureFlags()
	return args.fs.Usage()
}

func (s *Stalin) List(m *gateway.MessageCreateEvent, flags listFlags) (*discord.Embed, error) {
	settings, err := s.getSettings(m.GuildID)
	if err != nil {
		return nil, errors.New("blacklist is not initialized")
	}

	desc := strings.Join(settings.BlacklistedWords, "\n")
	if !flags.Uncensor {
		desc = censorVowels(desc)
	}

	return &discord.Embed{
		Title:       fmt.Sprintf("%d words blacklisted", len(settings.BlacklistedWords)),
		Description: desc,
	}, nil
}

func (s *Stalin) Censor(m *gateway.MessageCreateEvent, args *bot.ArgumentParts) (string, error) {
	settings, err := s.getSettings(m.GuildID)
	// If the guild hasn't been initialized before:
	if err != nil {
		settings = &Settings{}
	}

	// Check if the word is in the list. If it is, remove it.
	index := -1
	word := args.Arg(0)

	for i, bl := range settings.BlacklistedWords {
		if bl == word {
			index = i
			break
		}
	}

	words := settings.BlacklistedWords
	msg := ""

	if index > -1 {
		// The word is in the list, remove it it:
		words = append(words[:index], words[index+1:]...)
		msg = "Deleted the word."
	} else {
		// The word isn't in the list, add it:
		words = append(words, word)
		msg = "Added the word."
	}

	// Check if we have no blacklisted words left:
	if len(words) == 0 {
		if err := s.node.Delete(m.GuildID.String()); err != nil {
			return "", errors.Wrap(err, "Failed to delete settings")
		}

		return "Deleted the word; no words remained.", nil
	}

	// Save the settings:
	settings.BlacklistedWords = words

	if err := s.node.Set(m.GuildID.String(), settings); err != nil {
		return "", errors.Wrap(err, "Failed to save settings")
	}

	return msg, nil
}

func (s *Stalin) Except(m *gateway.MessageCreateEvent, args *bot.ArgumentParts) error {
	settings, err := s.getSettings(m.GuildID)
	if err != nil {
		return errors.Wrap(err, "Failed to get settings")
	}

	if args.Length() == 0 {
		var ids = make([]string, len(settings.ExceptionChannels))
		for i, id := range settings.ExceptionChannels {
			ids[i] = "<#" + id.String() + ">"
		}

		e := discord.Embed{
			Title:       fmt.Sprintf("%d exception channels", len(ids)),
			Description: strings.Join(ids, "\n"),
		}

		_, err = s.Ctx.SendMessage(m.ChannelID, "", &e)
		return err
	}

	var chID arguments.ChannelMention
	chID.Parse(args.Arg(0))

	ch, err := s.Ctx.Channel(chID.ID())
	if err != nil {
		return errors.Wrap(err, "Error getting channel")
	}

	var index = -1

	for i, id := range settings.ExceptionChannels {
		if id == ch.ID {
			index = i
			break
		}
	}

	var ids = settings.ExceptionChannels

	if index > -1 {
		ids = append(ids[:index], ids[index+1:]...)
	} else {
		ids = append(ids, ch.ID)
	}

	settings.ExceptionChannels = ids

	if err := s.node.Set(m.GuildID.String(), settings); err != nil {
		return errors.Wrap(err, "Failed to save settings")
	}

	_, err = s.Ctx.SendMessage(m.ChannelID,
		"Added channel "+ch.Mention()+" into exceptions.", nil)
	return err
}
