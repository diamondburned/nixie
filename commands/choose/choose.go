package choose

import (
	"errors"
	"hash/fnv"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/gateway"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

type Choose struct {
	Ctx *bot.Context
}

func New() *Choose { return &Choose{} }

func (c *Choose) Setup(sub *bot.Subcommand) {
	sub.Description = "Bless RNG."
	sub.SanitizeMessage = dctools.EscapeMentions
	sub.SetPlumb(c.Choose)
}

const Delimiter = " or "

func (*Choose) Help() string {
	return "choose a" + Delimiter + "b"
}

func (c *Choose) Choose(m *gateway.MessageCreateEvent, args bot.RawArguments) (string, error) {
	choices := strings.Split(string(args), Delimiter)
	if len(choices) < 2 {
		return "", errors.New("only one choice given")
	}

	var hash = fnv.New32a()
	for _, choice := range choices {
		hash.Write([]byte(choice))
	}

	choice := int(hash.Sum32()) % len(choices)
	return capitalize(choices[choice]) + ".", nil
}

func capitalize(s string) string {
	s = strings.TrimSpace(s)

	// If the string has 0 or 1 character(s), then we can just upper-case
	// everything.
	if len(s) < 2 {
		return strings.ToUpper(s)
	}

	// Decode the first rune.
	r, sz := utf8.DecodeRune([]byte(s))

	// Return the first rune capitalized with the rest of the sentence.
	return string(unicode.ToUpper(r)) + s[sz:]
}
