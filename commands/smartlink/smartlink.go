package smartlink

import (
	"strings"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
)

type Smartlink struct {
	Ctx *bot.Context
}

func New() *Smartlink { return &Smartlink{} }

func (s *Smartlink) Setup(sub *bot.Subcommand) {
	sub.AddMiddleware("*", func(m *gateway.MessageCreateEvent) error {
		if err := s.handler(m); err != nil {
			return err
		}
		return bot.Break
	})
}

// handler handles incoming messages and parses them
func (s *Smartlink) handler(c *gateway.MessageCreateEvent) error {
	if c.Content == "" || c.Author.Bot {
		return nil
	}

	url := arguments.ParseMessageURL(strings.Fields(c.Content)[0])
	if url == nil {
		return nil
	}

	m, err := s.Ctx.Message(url.ChannelID, url.MessageID)
	if err != nil {
		return nil
	}

	var name = m.Author.Username

	if m, err := s.Ctx.Member(m.GuildID, m.Author.ID); err == nil {
		if m.Nick != "" {
			name += " (" + m.Nick + ")"
		}
	}

	embed := &discord.Embed{
		Author: &discord.EmbedAuthor{
			Name: name,
			Icon: m.Author.AvatarURL(),
		},
		Description: m.Content,
	}

	switch {
	case len(m.Attachments) > 0:
		a := m.Attachments[0]

		if a.Width > 0 && a.Height > 0 {
			embed.Image = &discord.EmbedImage{
				URL: a.URL,
			}
		}
	case len(m.Embeds) > 0:
		e := m.Embeds[0]

		switch {
		case e.Image != nil:
			embed.Image = e.Image
		case e.Thumbnail != nil:
			embed.Thumbnail = &discord.EmbedThumbnail{
				URL: e.Thumbnail.URL,
			}
		}
	}

	_, err = s.Ctx.SendMessage(c.ChannelID, "", embed)
	return err
}
