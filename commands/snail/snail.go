package snail

import (
	"strconv"
	"strings"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/utils/json/option"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
)

const Speed = 30 // seconds

type Snail struct {
	Ctx  *bot.Context
	node db.Node
}

func New(node db.Node) *Snail {
	return &Snail{
		node: node,
	}
}

type Guild struct {
	Channels []Channel
}

type Channel struct {
	ID       discord.ChannelID
	SlowMode uint32
}

func (c *Snail) Setup(sub *bot.Subcommand) {
	sub.Hidden = true
	// Plumb methods can be accessed directly:
	sub.AddMiddleware("*", middlewares.AdminOnly(c.Ctx))
	sub.SetPlumb("Toggle")
	sub.Commands[0].Arguments[0].String = "[enable|disable]"
	sub.Description = "Set a 30 seconds slow mode on all channels."
}

func (c *Snail) Toggle(m *gateway.MessageCreateEvent, args bot.RawArguments) (string, error) {
	var guild Guild
	guildErr := c.node.Get(m.GuildID.String(), &guild)

	// If Get failed, that means the guild does not exist, meaning it's not
	// slowed yet. Thus we enable Snail mode.
	var snail = guildErr != nil

	// Check if enabled or disabled is given.
	switch {
	case strings.HasSuffix(string(args), "enable"):
		// Don't allow them to call enable twice.
		if !snail {
			return "Guild is already snailed.", nil
		}
		snail = true
	case strings.HasSuffix(string(args), "disable"):
		snail = false
	}

	// If the user requests snail to be disabled:
	if !snail {
		if guildErr != nil {
			return "", errors.Wrap(guildErr, "Failed to get settings for guild")
		}

		c.Ctx.Typing(m.ChannelID)
		return c.disable(m.GuildID, &guild)
	} else {
		c.Ctx.Typing(m.ChannelID)
		return c.enable(m.GuildID)
	}
}

func (c *Snail) disable(guild discord.GuildID, s *Guild) (string, error) {
	var errs = make([]error, 0, len(s.Channels)/2) // 1 more alloc max, negligible

	for _, ch := range s.Channels {
		d := api.ModifyChannelData{
			UserRateLimit: option.NewNullableUint(uint(ch.SlowMode)),
		}
		if err := c.Ctx.ModifyChannel(ch.ID, d); err != nil {
			errs = append(errs, fmtChErr(ch.ID, err, "Failed to restore"))
		}
	}

	// Delete the guild settings, ignoring the error because it's not important.
	c.node.Delete(guild.String())

	var msg = "Snail mode disabled for " + strconv.Itoa(len(s.Channels)) + " channels." +
		tailErrors(errs)

	return msg, nil
}

func (c *Snail) enable(guild discord.GuildID) (string, error) {
	// can't rely on state :(
	channels, err := c.Ctx.Client.Channels(guild)
	if err != nil {
		return "", errors.Wrap(err, "Failed to get channels")
	}

	u, err := c.Ctx.Me()
	if err != nil {
		return "", errors.Wrap(err, "Failed to get self info")
	}

	var errs = make([]error, 0, len(channels)/2) // 1 more alloc max, negligible
	var data = api.ModifyChannelData{
		UserRateLimit: option.NewNullableUint(uint(Speed)),
	}
	var s = Guild{
		Channels: make([]Channel, 0, len(channels)),
	}

	for _, ch := range channels {
		// Only apply on text channels:
		if ch.Type != discord.GuildText {
			continue
		}

		// Check if we have permissions:
		p, err := c.Ctx.Permissions(ch.ID, u.ID)
		if err != nil {
			errs = append(errs, fmtChErr(ch.ID, err, "Failed to get permissions"))
			continue
		}
		if !p.Has(discord.PermissionManageChannels) {
			errs = append(errs, fmtChErr(ch.ID, nil, "No permission"))
			continue
		}

		// Apply slow mode:
		if err := c.Ctx.ModifyChannel(ch.ID, data); err != nil {
			errs = append(errs, fmtChErr(ch.ID, err, "Failed to modify channel"))
			continue
		}

		s.Channels = append(s.Channels, Channel{
			ID:       ch.ID,
			SlowMode: uint32(ch.UserRateLimit),
		})
	}

	if err := c.node.Set(guild.String(), s); err != nil {
		return "", errors.Wrap(err, "Failed to save settings")
	}

	var msg = "Snail mode enabled for " + strconv.Itoa(len(s.Channels)) + " channels." +
		tailErrors(errs)

	return msg, nil
}

func fmtChErr(chID discord.ChannelID, err error, msg string) error {
	if err == nil {
		return errors.New(msg + " for <#" + chID.String() + ">")
	}
	return errors.Wrap(err, msg+" for <#"+chID.String()+">")
}

func tailErrors(errs []error) string {
	var msg = "\n"
	for _, err := range errs {
		msg += "\n" + err.Error()
	}
	return msg
}
