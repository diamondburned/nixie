package starboard

import (
	"fmt"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/api/webhook"
	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

const MaxStarred = 200

type Starboard struct {
	Ctx  *bot.Context
	node db.Node
	// - settings: *GuildSettings
	// - messages: node
	//     - Snowflakes: *Starred
}

func New(node db.Node) *Starboard {
	// errors are log.Fatalln-able
	return &Starboard{
		node: node,
	}
}

type GuildSettings struct {
	GuildID discord.GuildID

	WebhookID      discord.WebhookID
	WebhookToken   string
	WebhookChannel discord.ChannelID

	Starcount uint32
	Reaction  string

	Blacklisted []discord.ChannelID
}

func (s *Starboard) Setup(sub *bot.Subcommand) {
	sub.Description = "Where the best messages go."
	sub.Hidden = true
	dctools.ApplyMiddlewares(
		sub,
		middlewares.AdminOnly(s.Ctx),
		"Initialize",
		"SetReaction",
		"SetStarcount",
		"GetBlacklisted",
		"ToggleBlacklisted",
		"ResetMessage",
	)
}

func (s *Starboard) Initialize(
	m *gateway.MessageCreateEvent, mention *arguments.ChannelMention) error {

	ch, err := s.Ctx.Channel(mention.ID())
	if err != nil {
		return errors.Wrap(err, "Failed to get mentioned channel")
	}

	node := s.node.Node(ch.GuildID.String())

	var gs = GuildSettings{}

	if err := node.Get("settings", &gs); err != nil {
		// Drop sent messages and settings.
		node.Drop()

		// Delete the old webhook.
		webhook.New(gs.WebhookID, gs.WebhookToken).Delete()
	}

	// Make a webhook name with maximum 32 characters.
	var webhookName = "Starboard hook: " + ch.Name
	if len(webhookName) > 32 {
		webhookName = webhookName[:29] + "..."
	}

	w, err := s.Ctx.CreateWebhook(ch.ID, api.CreateWebhookData{Name: webhookName})
	if err != nil {
		return errors.Wrap(err, "Failed to create a webhook")
	}

	gs.GuildID = ch.GuildID
	gs.WebhookID = w.ID
	gs.WebhookToken = w.Token
	gs.WebhookChannel = ch.ID
	gs.Starcount = 4
	gs.Reaction = "\u2b50" // default star

	if err := node.Set("settings", gs); err != nil {
		return errors.Wrap(err, "Failed to save settings")
	}

	_, err = s.Ctx.SendMessage(m.ChannelID, "Setup completed.", nil)
	return nil
}

func (s *Starboard) GetReaction(m *gateway.MessageCreateEvent) error {
	g, err := s.getSettings(m.GuildID)
	if err != nil {
		return err
	}

	_, err = s.Ctx.SendMessage(m.ChannelID, "Reaction: "+g.Reaction, nil)
	return err
}

func (s *Starboard) SetReaction(
	m *gateway.MessageCreateEvent, emoji string) error {

	if emoji != "*" {
		e := arguments.Emoji{}
		if err := e.Parse(emoji); err != nil {
			return errors.Wrap(err, "Failed to parse emoji")
		}

		emoji = e.APIString()
	}

	return s.changeSettings(m, "Changed emoji to: "+emoji,
		func(gs *GuildSettings) {
			gs.Reaction = emoji
		},
	)
}

func (s *Starboard) GetStarcount(m *gateway.MessageCreateEvent) error {
	g, err := s.getSettings(m.GuildID)
	if err != nil {
		return err
	}

	_, err = s.Ctx.SendMessage(m.ChannelID,
		fmt.Sprintf("Starcount: %d", g.Starcount), nil)
	return err
}

func (s *Starboard) SetStarcount(
	m *gateway.MessageCreateEvent, count uint32) error {

	return s.changeSettings(m, fmt.Sprintf("Changed starcount to: %d", count),
		func(gs *GuildSettings) {
			gs.Starcount = count
		},
	)
}

func (s *Starboard) GetBlacklisted(m *gateway.MessageCreateEvent) error {
	g, err := s.getSettings(m.GuildID)
	if err != nil {
		return err
	}

	var msg = "Blacklisted channels: "
	for _, id := range g.Blacklisted {
		msg += "<#" + id.String() + ">, "
	}

	_, err = s.Ctx.SendMessage(m.ChannelID, msg, nil)
	return err
}

func (s *Starboard) ToggleBlacklisted(
	m *gateway.MessageCreateEvent, ch *arguments.ChannelMention) error {

	g, err := s.getSettings(m.GuildID)
	if err != nil {
		return err
	}

	var msg string

	// Try and find the channel:
	for i, id := range g.Blacklisted {
		if id == ch.ID() {
			// Start deleting stuff
			sl := g.Blacklisted
			sl = append(sl[:i], sl[i+1:]...)

			g.Blacklisted = sl
			msg = "Removed channel: " + ch.Mention()

			break
		}
	}

	// Channel not found, so add it:
	if msg == "" {
		g.Blacklisted = append(g.Blacklisted, ch.ID())
		msg = "Added channel: " + ch.Mention()
	}

	if err := s.node.Node(m.GuildID.String()).Set("settings", g); err != nil {
		return errors.Wrap(err, "Can't save guild settings")
	}

	_, err = s.Ctx.SendMessage(m.ChannelID, msg, nil)
	return err
}

func (s *Starboard) ResetMessage(
	m *gateway.MessageCreateEvent, msgID discord.Snowflake) error {

	// Delete from guild node.
	if err := s.node.Node(m.GuildID.String()).Delete(msgID.String()); err != nil {
		return errors.New("Message ID not found.")
	}

	return nil
}

func (s *Starboard) getSettings(guild discord.GuildID) (*GuildSettings, error) {
	var gs *GuildSettings

	if err := s.node.Node(guild.String()).Get("settings", &gs); err != nil {
		return nil, errors.Wrap(err, "Can't get guild settings")
	}

	return gs, nil
}

func (s *Starboard) changeSettings(
	m *gateway.MessageCreateEvent, msg string, fn func(*GuildSettings)) error {

	g, err := s.getSettings(m.GuildID)
	if err != nil {
		return err
	}

	// Call the function that updates *GuildSettings
	fn(g)

	if err := s.node.Node(m.GuildID.String()).Set("settings", g); err != nil {
		return errors.Wrap(err, "Can't save guild settings")
	}

	_, err = s.Ctx.SendMessage(m.ChannelID, msg, nil)
	return err
}
