package starboard

import (
	"fmt"
	"strings"
	"time"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/api/webhook"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
)

// Starred messages expire after 2 days.
const Expiry = 48 * time.Hour

// func (s *Starboard) cleanup(guildNode db.Node) error {
// 	var node = guildNode.Node("messages")
// 	var less = discord.NewSnowflake(time.Now().Add(-48 * time.Hour))

// 	var IDs []string
// 	var st *Starred

// 	err := node.Each(&st, "", func(k string) error {
// 		// If ID is less than a fake ID that would imply messages older than 2
// 		// days ago:
// 		if st.ID < less {
// 			IDs = append(IDs, k)
// 		}
// 		return nil
// 	})

// 	if err != nil {
// 		return err
// 	}

// 	for _, id := range IDs {
// 		if e := node.Delete(id); e != nil {
// 			err = e
// 		}
// 	}

// 	return err
// }

func (s *Starboard) Handler(r *gateway.MessageReactionAddEvent) error {
	// Get the guild node.
	var guildNode = s.node.Node(r.GuildID.String())

	// Check if the guild is in the database:
	var g *GuildSettings
	if err := guildNode.Get("settings", &g); err != nil {
		// Drop the event if we can't find the guild.
		return nil
	}

	for _, c := range g.Blacklisted {
		if c == r.ChannelID {
			// Channel is blacklisted, drop the event:
			return nil
		}
	}

	// Support for wildcard emojis, aka any reaction.
	if g.Reaction != "*" && g.Reaction != r.Emoji.Name {
		// Not the reaction we're looking for, drop it.
		return nil
	}

	// Is the message already starred?
	if err := guildNode.Get(r.MessageID.String(), &r.ChannelID); err == nil {
		// No errors, found message.
		return nil
	}

	// Get message from either the state cache or the channel:
	m, err := s.Ctx.Message(r.ChannelID, r.MessageID)
	if err != nil {
		return errors.Wrap(err, "Failed to get the message")
	}

	// 48 hours == 2 days
	// Drop all messages older than 2 days:
	if m.Timestamp.Time().Add(Expiry).Before(time.Now()) {
		return nil
	}

	// If the reaction is from the message author:
	if r.UserID == m.Author.ID {
		// we delete it. Seriously. Why would you do this? Only when we know
		// we don't accept any emojis though.
		if g.Reaction != "*" {
			// Error is not caught intentionally
			s.Ctx.DeleteUserReaction(
				r.ChannelID, r.MessageID,
				m.Author.ID, discord.APIEmoji(g.Reaction),
			)
		}

		return nil
	}

	// Get a list of users so we could count them (we don't actually need all
	// users who reacted):
	u, err := s.Ctx.Reactions(
		// Starcount is added 1 to account for the message author.
		r.ChannelID, r.MessageID, r.Emoji.APIString(), uint(g.Starcount+1))
	if err != nil {
		return errors.Wrap(err, "Failed to get reactions")
	}

	// We have to see if the user reacted to their message:
	var reacts = len(u)

	for _, user := range u {
		if user.ID == m.Author.ID {
			// Subtract 1 from the count:
			reacts--
			break
		}
	}

	// If the reaction count is not equal, drop it
	if reacts < int(g.Starcount) {
		return nil
	}

	// There's no need to try and garbage collect starred messages. Badger
	// already has a TTL API.

	// By this point, we're certain that the message should be
	// starred, as it satisfies the conditions that the emoji
	// and the count match what we want.

	var nickname = m.Author.Username

	// Doing all this for a string of nickname
	if m, err := s.Ctx.Member(r.GuildID, m.Author.ID); err == nil {
		if m.Nick != "" {
			nickname = m.Nick
		}
	}

	var embeds = make([]discord.Embed, 0, len(m.Attachments))
	for _, a := range m.Attachments {

		if len(embeds) < 10 && a.Width > 0 && a.Height > 0 &&
			hasSuffices(a.URL, "png", "jpg", "jpeg", "gif") {

			embeds = append(embeds, discord.Embed{
				Image: &discord.EmbedImage{
					URL:   a.URL,
					Proxy: a.Proxy,
				},
			})

			continue
		}

		m.Content += "\n" + a.URL
	}

	// The message content, copied for mutating.
	var content = m.Content

	// There are some cases where we need to do things to the content, such as
	// when the message is a join message.
	switch m.Type {
	case discord.GuildMemberJoinMessage:
		content = m.Author.Mention() + " joined the server."
	case discord.ChannelIconChangeMessage:
		content = m.Author.Mention() + " changed the channel icon."
	case discord.ChannelNameChangeMessage:
		content = m.Author.Mention() + " changed the channel name to " +
			content + "."
	case discord.ChannelPinnedMessage:
		content = m.Author.Mention() + " pinned message " + m.URL() + "."
	}

	// We prepend an emoji at the start, if the guild accepts any reaction.
	if g.Reaction == "*" {
		var emoji string

		if !r.Emoji.ID.IsValid() {
			emoji = r.Emoji.Name
		} else {
			// Try and see if the emoji is in the guild we're in:
			g, _ := s.Ctx.Guild(r.GuildID)

			// emojiInGuild already does nil check.
			if e := emojiInGuild(g, r.Emoji.ID); e != nil {
				// Emoji is in the guild, we're gonna use it.
				emoji = r.Emoji.String()
			} else {
				// Emoji isn't, we can't do much here.
				emoji = fmt.Sprintf("[emoji](%s)", emojiURL(r.Emoji))
			}
		}

		content = emoji + " | " + content
	}

	webhookData := webhook.ExecuteData{
		Username:  nickname,
		Content:   content + " [*(source)*](<" + m.URL() + ">)",
		AvatarURL: m.Author.AvatarURL(),
		Embeds:    embeds,
		AllowedMentions: &api.AllowedMentions{
			// Mention no one.
			Parse: []api.AllowedMentionType{},
		},
	}

	if err = webhook.New(g.WebhookID, g.WebhookToken).Execute(webhookData); err != nil {
		return err
	}

	// Add the message. We use the message ID as the key and set the channel ID
	// for the value.
	if err := guildNode.SetWithTTL(m.ID.String(), m.ChannelID, Expiry); err != nil {
		return errors.Wrap(err, "Failed to save the message")
	}

	return nil
}

func emojiInGuild(g *discord.Guild, emojiID discord.EmojiID) *discord.Emoji {
	if g == nil {
		return nil
	}

	for _, e := range g.Emojis {
		if e.ID == emojiID {
			return &e
		}
	}

	return nil
}

func hasSuffices(str string, suffices ...string) bool {
	str = strings.ToLower(str)

	for _, suffix := range suffices {
		if strings.HasSuffix(str, suffix) {
			return true
		}
	}

	return false
}

func emojiURL(e discord.Emoji) string {
	base := "https://cdn.discordapp.com/emojis/" + e.ID.String()
	if e.Animated {
		return base + ".gif"
	} else {
		return base + ".png"
	}
}
