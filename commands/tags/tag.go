package tags

import (
	"github.com/diamondburned/arikawa/v2/discord"
)

type Tag struct {
	Content     string
	Attachments []discord.URL

	AuthorID  discord.UserID
	Timestamp int64 // UnixNano
}

func TagMessage(m discord.Message, content string) Tag {
	t := Tag{
		Content: content,
		Attachments: make(
			[]discord.URL, 0, len(m.Attachments),
		),
		AuthorID:  m.Author.ID,
		Timestamp: m.Timestamp.Time().UnixNano(),
	}

	for _, a := range m.Attachments {
		t.Attachments = append(t.Attachments, a.URL)
	}

	for _, e := range m.Embeds {
		if e.Type == discord.ImageEmbed {
			t.Attachments = append(t.Attachments, e.URL)
		}
	}

	return t
}

func (t Tag) Valid() bool {
	return !(t.Content == "" && len(t.Attachments) == 0)
}

func (t Tag) Owns(user discord.UserID) bool {
	return t.AuthorID == user
}
