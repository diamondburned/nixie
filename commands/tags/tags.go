package tags

import (
	"strconv"
	"strings"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

type Tags struct {
	Ctx  *bot.Context
	node db.Node
}

func New(node db.Node) *Tags {
	return &Tags{
		node: node,
	}
}

func (t *Tags) Setup(sub *bot.Subcommand) {
	sub.Description = "Glorified server-wide key-value store."
	sub.SanitizeMessage = dctools.EscapeMentions
	sub.AddMiddleware("*", middlewares.GuildOnly(t.Ctx))

	for _, cmd := range sub.Commands {
		switch cmd.MethodName {
		case "Set":
			cmd.Arguments[0].String = "name"
			cmd.Arguments[1].String = "content... [attachments...]"
			cmd.Description = "Set a tag."
		case "Get":
			cmd.Arguments[0].String = "name"
			cmd.Description = "Get a tag."
		case "Del":
			cmd.Arguments[0].String = "name"
			cmd.Description = "Delete a tag."
		case "Search":
			cmd.Arguments[0].String = "string"
			cmd.Description = "Search any tag."
		}
	}

	sub.SetPlumb(t.Get)
}

func (t *Tags) Set(m *gateway.MessageCreateEvent, name string, c bot.ArgumentParts) (string, error) {
	// sanitize name because we can't trust people
	name = dctools.Sanitize(name)

	if tag, err := t.getTag(m.GuildID, name); err == nil {
		if !t.ownsTag(tag, m.ChannelID, m.Author.ID) {
			return "", errors.New("Tag is not yours.")
		}
	}

	var tag = TagMessage(m.Message, strings.Join([]string(c), " "))
	if !tag.Valid() {
		return "", errors.New("Missing tag content.")
	}

	if err := t.node.Node(m.GuildID.String()).Set(name, tag); err != nil {
		return "", errors.Wrap(err, "Failed to save")
	}

	return "Tag set to: " + name, nil
}

func (t *Tags) Get(m *gateway.MessageCreateEvent, name arguments.Joined) (string, error) {
	if name == "" {
		return "", errors.New("Name not given. Usage: `get <name>`")
	}

	tag, err := t.getTag(m.GuildID, string(name))
	if err != nil {
		return "", errors.Wrap(err, "Failed to get tag")
	}

	for _, url := range tag.Attachments {
		tag.Content += "\n" + url
	}

	return tag.Content, nil
}

func (t *Tags) Del(m *gateway.MessageCreateEvent, name string) error {
	tag, err := t.getTag(m.GuildID, name)
	if err != nil {
		return errors.Wrap(err, "Failed to get tag")
	}

	if !t.ownsTag(tag, m.ChannelID, m.Author.ID) {
		return errors.New("Tag is not yours.")
	}

	if err := t.node.Node(m.GuildID.String()).Delete(name); err != nil {
		return errors.Wrap(err, "Failed to delete tag")
	}

	_, err = t.Ctx.SendMessage(m.ChannelID, "Deleted.", nil)
	return err
}

func (t *Tags) Search(m *gateway.MessageCreateEvent, args bot.ArgumentParts) error {
	match := args.Arg(0)

	const max = 15
	var index, sum int

	var embeds []discord.Embed
	var embed = &discord.Embed{
		Color: 0x2e5ed2,
	}

	err := t.node.Node(m.GuildID.String()).EachKey("", func(k string) error {
		// Check for matching:
		if match != "" && !strings.Contains(k, match) {
			return nil
		}

		// Append to the description
		embed.Description += "`" + k + "`\n"

		// Increment the index and sum
		index++
		sum++

		// Trim the embed to a new one if we've reached entry limit:
		if index >= max {
			// Reset the index counter
			index = 0

			// Create a new embed
			embeds = append(embeds, *embed)

			// Reset the current embed
			embed.Description = ""
		}

		return nil
	})

	if err != nil {
		return err
	}

	// The last embed is still not added, we should do that.
	if index > 0 && embed.Description != "" {
		embeds = append(embeds, *embed)
	}

	// If nothing was found:
	if len(embeds) == 0 {
		return errors.New("No tags found.")
	}

	// Set all the embed's titles:
	title := strconv.Itoa(sum) + " tags found"
	for i := range embeds {
		embeds[i].Title = title
	}

	// We don't need the paginator if there's only 1 page:
	if len(embeds) == 1 {
		_, err := t.Ctx.SendMessage(m.ChannelID, "", &embeds[0])
		return err
	}

	p := dctools.DefaultPaginator(t.Ctx.State, *m)
	p.Add(embeds...)
	p.SetPageFooters()

	// Spawn:
	return p.Spawn()
}

func (t *Tags) ownsTag(tag *Tag, channel discord.ChannelID, user discord.UserID) bool {
	if tag.Owns(user) {
		return true
	}

	p, err := t.Ctx.Permissions(channel, user)
	if err != nil {
		return false
	}

	return p.Has(discord.PermissionAdministrator)
}

func (t *Tags) getTag(guild discord.GuildID, name string) (tag *Tag, err error) {
	node := t.node.Node(guild.String())

	if err = node.Get(name, &tag); err != nil {
		if !errors.Is(err, db.ErrKeyNotFound) {
			node.Delete(name)
		}

		return nil, err
	}

	return tag, nil
}
