package buttkick

import (
	"math/rand"
	"testing"
	"time"

	"github.com/diamondburned/arikawa/v2/discord"
)

func randUserID() discord.UserID {
	return discord.UserID(rand.Uint64())
}

func randJoin() Join {
	return Join{
		ID:   randUserID(),
		Time: time.Now().Add(-time.Duration(rand.Intn(360)) * time.Second),
	}
}

func TestJoins(t *testing.T) {
	var joins = Joins{}
	var setts = Settings{
		Duration:  365 * time.Second,
		JoinLimit: 5,
	}

	for i := range joins.List {
		joins.List[i] = randJoin()
	}

	// Pop BucketSize*2 times.
	for i := 0; i < bucketCap*2; i++ {
		joins.add(setts, randJoin())
	}

	// Do a length check.
	if len(joins.List) != bucketCap {
		t.Errorf("Length mismatch: expected %d, got %d", bucketCap, len(joins.List))
	}
}
