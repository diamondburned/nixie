package buttkick

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
	"gitlab.com/diamondburned/nixie/logger"
)

type Buttkick struct {
	Ctx  *bot.Context
	node db.Node
	Repo JoinRepository
}

type Settings struct {
	Paused     bool
	LogChannel discord.ChannelID
	Duration   time.Duration
	JoinLimit  int
}

const maxDuration = 7 * 24 * time.Hour // 7 days

func New(node db.Node) *Buttkick {
	return &Buttkick{node: node}
}

func (b *Buttkick) Setup(sub *bot.Subcommand) {
	sub.Hidden = true
	sub.AddMiddleware(b.Enable, middlewares.AdminOnly(b.Ctx))
	sub.AddMiddleware(b.Disable, middlewares.AdminOnly(b.Ctx))
	sub.AddMiddleware(b.Pause, middlewares.AdminOnly(b.Ctx))
	sub.AddMiddleware(b.SetDuration, middlewares.AdminOnly(b.Ctx))
	sub.AddMiddleware(b.SetJoinLimit, middlewares.AdminOnly(b.Ctx))

	sub.ChangeCommandInfo(b.Info, "", "show bucket information for current guild")
	sub.ChangeCommandInfo(b.SetDuration, "", "the duration for each bucket")
	sub.ChangeCommandInfo(b.SetJoinLimit, "", "the join limit or size for each bucket")
	sub.ChangeCommandInfo(b.Pause, "", "pause buttkicking")

	sub.FindCommand(b.Enable).Arguments[0].String = "#log-channel"
	sub.FindCommand(b.SetDuration).Arguments[0].String = "duration"
	sub.FindCommand(b.SetJoinLimit).Arguments[0].String = "join-limit"
}

func (b *Buttkick) Enable(
	m *gateway.MessageCreateEvent, ch arguments.ChannelMention) (string, error) {

	err := b.node.Set(m.GuildID.String(), Settings{
		LogChannel: ch.ID(),
		// 5 joins per 3 seconds.
		Duration:  3 * time.Second,
		JoinLimit: 5,
	})

	if err != nil {
		return "", errors.Wrap(err, "Failed to save changes")
	}

	return "Buttkick log set to " + ch.Mention() + ".", nil
}

func (b *Buttkick) Disable(m *gateway.MessageCreateEvent) (string, error) {
	err := b.node.Delete(m.GuildID.String())
	if err != nil {
		return "", errors.Wrap(err, "Failed to delete settings")
	}

	b.Repo.Delete(m.GuildID)

	return "Deleted settings.", nil
}

func (b *Buttkick) Pause(m *gateway.MessageCreateEvent, pause bool) (string, error) {
	var settings Settings
	if err := b.node.Get(m.GuildID.String(), &settings); err != nil {
		return "", errors.Wrap(err, "failed to get settings")
	}

	settings.Paused = pause

	if err := b.node.Set(m.GuildID.String(), settings); err != nil {
		return "", errors.Wrap(err, "Failed to save changes")
	}

	if pause {
		return "Paused buttkicking.", nil
	}

	return "Resumed buttkicking.", nil
}

func (b *Buttkick) SetDuration(m *gateway.MessageCreateEvent, d dctools.Duration) (string, error) {
	var settings Settings
	if err := b.node.Get(m.GuildID.String(), &settings); err != nil {
		return "", errors.Wrap(err, "failed to get settings")
	}

	settings.Duration = time.Duration(d)

	switch {
	case settings.Duration > maxDuration:
		return "", errors.New("duration is too long")
	case settings.Duration < 0:
		return "", errors.New("negative duration not allowed")
	}

	if err := b.node.Set(m.GuildID.String(), settings); err != nil {
		return "", errors.Wrap(err, "Failed to save changes")
	}

	return "Set the duration to " + settings.Duration.String() + ".", nil
}

func (b *Buttkick) SetJoinLimit(m *gateway.MessageCreateEvent, l int) (string, error) {
	var settings Settings
	if err := b.node.Get(m.GuildID.String(), &settings); err != nil {
		return "", errors.Wrap(err, "failed to get settings")
	}

	settings.JoinLimit = l

	switch {
	case settings.JoinLimit > bucketCap:
		return "", fmt.Errorf("join-limit is too large (max: %d)", bucketCap)
	case settings.Duration < 0:
		return "", errors.New("negative join-limit not allowed")
	}

	if err := b.node.Set(m.GuildID.String(), settings); err != nil {
		return "", errors.Wrap(err, "Failed to save changes")
	}

	return "Set the join limit to " + strconv.Itoa(l) + ".", nil
}

func (b *Buttkick) Info(m *gateway.MessageCreateEvent) (*discord.Embed, error) {
	var settings Settings
	if err := b.node.Get(m.GuildID.String(), &settings); err != nil {
		return nil, errors.New("Guild is not in the database. Try re-enabling.")
	}

	return &discord.Embed{
		Color: 0xAEC6CF, // Pastel Blue
		Title: "Buttkick Status",
		Description: fmt.Sprintf(
			"Bucket\n%d joins per %v", settings.JoinLimit, settings.Duration,
		),
		Fields: []discord.EmbedField{
			{Name: "Paused", Value: strconv.FormatBool(settings.Paused)},
			{Name: "Duration", Value: settings.Duration.String(), Inline: true},
			{Name: "Join Limit", Value: strconv.Itoa(settings.JoinLimit), Inline: true},
		},
	}, nil
}

func (b *Buttkick) MemberJoin(c *gateway.GuildMemberAddEvent) {
	var settings Settings
	if err := b.node.Get(c.GuildID.String(), &settings); err != nil {
		return
	}

	joins := b.Repo.Joins(c.GuildID)

	joins.Lock()

	joins.Add(settings, c)

	// Copy for thread safety.
	var toKick []Join
	if bucket := joins.LastBucket(settings); len(bucket) > 0 {
		toKick = append(toKick, bucket...)
	}

	joins.Unlock()

	if toKick == nil {
		return
	}

	kicked := b.kick(c.GuildID, toKick)

	builder := strings.Builder{}
	builder.WriteString("Kicked: ")

	for i, join := range kicked {
		if i > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(join.Name)
	}

	logger.Log(b.Ctx, c.GuildID, builder.String(), nil)
}

const Reason = "Automatically kicked by Buttkicker (nixie)."

// kick kicks people and returns a list of people kicked.
func (b *Buttkick) kick(guildID discord.GuildID, toKick []Join) []Join {
	var kicked = toKick[:0]

	for _, join := range toKick {
		if err := b.Ctx.KickWithReason(guildID, join.ID, Reason); err != nil {
			logger.Error(b.Ctx, guildID, errors.Wrap(err, "failed to autokick"))
		}

		kicked = append(kicked, join)
	}

	return kicked
}
