package buttkick

import (
	"fmt"
	"sync"
	"time"

	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
)

const bucketCap = 200

type JoinRepository struct {
	sync.Map
}

func (r *JoinRepository) Joins(guildID discord.GuildID) *Joins {
	v, _ := r.Map.LoadOrStore(guildID, &Joins{})
	return v.(*Joins)
}

func (r *JoinRepository) Delete(guildID discord.GuildID) {
	r.Map.Delete(guildID)
}

type Joins struct {
	sync.Mutex
	List []Join
}

type Join struct {
	ID   discord.UserID
	Time time.Time
	Name string // username#discrim
}

func (j *Joins) LastBucket(s Settings) []Join {
	tail := len(j.List) - 1
	head := tail
	now := time.Now()
	buc := now.Add(-s.Duration)

	for _, join := range j.List {
		if join.Time.Before(buc) {
			break
		}

		head--
	}

	// If the bucket isn't large enough, then we return nothing. Once it is
	// large enough, we'll kick everyone.

	if tail-head < s.JoinLimit {
		return nil
	}

	return j.List[head : len(j.List)-1]
}

func (j *Joins) Add(s Settings, member *gateway.GuildMemberAddEvent) {
	j.add(s, Join{
		ID:   member.User.ID,
		Time: member.Joined.Time(),
		Name: fmt.Sprintf("%s#%s", member.User.Username, member.User.Discriminator),
	})
}

func (j *Joins) add(s Settings, join Join) {
	j.List = append(j.List, join)

	if len(j.List) > bucketCap {
		head := len(j.List) - bucketCap
		tail := len(j.List) - head

		copy(j.List, j.List[head:len(j.List)])
		j.List = j.List[:tail]
	}
}
