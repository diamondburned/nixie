package expref

import (
	"fmt"
	"strings"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/diamondburned/arikawa/v2/utils/json/option"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
)

type ExpRef struct {
	Ctx  *bot.Context
	node db.Node
}

type settings struct{}

func New(n db.Node) *ExpRef {
	return &ExpRef{
		node: n,
	}
}

func (e *ExpRef) Setup(sub *bot.Subcommand) {
	sub.Hidden = true

	sub.AddMiddleware("*", middlewares.GuildOnly(e.Ctx))
	sub.AddMiddleware("*", e.onMessage)

	sub.AddMiddleware(e.Enable, middlewares.AdminOnly(e.Ctx))
	sub.ChangeCommandInfo(e.Enable, "", "enable or disable ExpRef")
}

func (e *ExpRef) onMessage(m *gateway.MessageCreateEvent) error {
	if m.Content == "" || m.Author.Bot || m.Type != discord.InlinedReplyMessage {
		return nil
	}

	// Message is deleted if ReferencedMessage is nil.
	if m.ReferencedMessage == nil {
		return nil
	}

	// Check if we have the guild in the database.
	var s settings
	if err := e.node.Get(m.GuildID.String(), &s); err != nil {
		return nil
	}

	echo, err := e.Ctx.SendMessageComplex(m.ChannelID, api.SendMessageData{
		Content: fmt.Sprintf(
			"%s's latest message replies to %s's message:\n%s",
			mention(m.Author),
			mention(m.ReferencedMessage.Author),
			quoteMsg(m.ReferencedMessage.Content, 100),
		),
		AllowedMentions: &api.AllowedMentions{
			// Parse no mentions.
			Parse:       []api.AllowedMentionType{},
			Roles:       []discord.RoleID{},
			Users:       []discord.UserID{},
			RepliedUser: option.False,
		},
	})

	if err != nil {
		return nil
	}

	// Suppress embeds after sending them. We're avoiding a number of possible
	// different solutions here:
	//
	//   - Looking for embeds: some embeds might appear in a MessageUpdate, so
	//     it's not reliable.
	//   - Using regex: I'm too lazy.
	//

	e.Ctx.EditMessageComplex(m.ChannelID, echo.ID, api.EditMessageData{
		Flags: &discord.SuppressEmbeds,
	})
	return nil
}

func mention(user discord.User) string {
	if user.Discriminator == "0000" {
		return user.Username
	}

	return "<@!" + user.ID.String() + ">"
}

var noBlockReplacer = strings.NewReplacer(
	"\n", "  ",
	"\t", "  ",
	"```", "`",
	">", "\\>",
)

func quoteMsg(msg string, max int) string {
	var runes = []rune(msg)
	if len(runes) > max {
		runes = append(runes[:max-1], []rune("…")...)
	}

	return "> " + noBlockReplacer.Replace(string(runes))
}

func (e *ExpRef) Enable(m *gateway.MessageCreateEvent, enable bool) (string, error) {
	var err error
	if enable {
		err = e.node.Set(m.GuildID.String(), settings{})
	} else {
		err = e.node.Delete(m.GuildID.String())
	}

	if err != nil {
		return "", errors.Wrap(err, "failed to save expref settings")
	}

	return "ExpRef settings applied.", nil
}
