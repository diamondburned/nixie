package aoc

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
)

type Leaderboard struct {
	OwnerID int64  `json:"owner_id"`
	Event   string `json:"event"`

	Members map[int64]LeaderboardMember `json:"members"`
}

type LeaderboardMember struct {
	ID            int64         `json:"id"`
	Name          string        `json:"name"`
	LocalScore    int64         `json:"local_score"`
	Stars         int64         `json:"stars"`
	DaysCompleted DaysCompleted `json:"completion_day_level"`
}

// Highest to lowest
func (l Leaderboard) SortMemberScores() []LeaderboardMember {
	var ms = make([]LeaderboardMember, 0, len(l.Members))
	for _, m := range l.Members {
		ms = append(ms, m)
	}

	sort.Slice(ms, func(i, j int) bool {
		return ms[i].LocalScore > ms[j].LocalScore
	})

	sort.SliceStable(ms, func(i, j int) bool {
		return ms[i].Stars > ms[j].Stars
	})

	return ms
}

func (s *Session) LeaderboardURL() string {
	return fmt.Sprintf(URL+"%d/leaderboard/private/view/%s", s.Year, s.ID())
}

func (s *Session) GetLeaderboard() (*Leaderboard, error) {
	r, err := s.get("/leaderboard/private/view/" + s.ID() + ".json")
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	return unmarshalLeaderboard(r.Body)
}

func unmarshalLeaderboard(r io.Reader) (*Leaderboard, error) {
	var l Leaderboard

	if err := json.NewDecoder(r).Decode(&l); err != nil {
		return nil, err
	}

	return &l, nil
}
