package aoc

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	URL     = "https://adventofcode.com/"
	Favicon = URL + "favicon.png"
)

var Client = http.Client{
	Timeout: 5 * time.Second,
}

type Session struct {
	Token string
	Year  int

	// xxxxxx-xxxxxxxx
	Code string
}

func New(year int, leaderboardCode string) (*Session, error) {
	if strings.Count(leaderboardCode, "-") != 1 {
		return nil, errors.New("invalid leaderboard code given")
	}

	return &Session{
		Year: year,
		Code: leaderboardCode,
	}, nil
}

// ID returns the first part of the leaderboard code.
func (s *Session) ID() string {
	return strings.Split(s.Code, "-")[0]
}

// "https://adventofcode.com/2019/" + URL
func (s *Session) get(url string) (*http.Response, error) {
	if s.Token == "" {
		return nil, errors.New("missing token")
	}

	r, err := http.NewRequest("GET", URL+strconv.Itoa(s.Year)+url, nil)
	if err != nil {
		return nil, err
	}

	r.AddCookie(&http.Cookie{Name: "session", Value: s.Token})

	return Client.Do(r)
}
