package aoc

import (
	"log"
	"strings"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestUnmarshalLeaderboard(t *testing.T) {
	l, err := unmarshalLeaderboard(strings.NewReader(testLeaderboardJSON))
	if err != nil {
		t.Fatal(err)
	}

	log.Println(l.Event)

	spew.Dump(l.SortMemberScores())
}

const testLeaderboardJSON = `{
  "event": "2022",
  "members": {
    "715845": {
      "local_score": 10,
      "last_star_ts": 1669871420,
      "id": 715845,
      "global_score": 0,
      "stars": 1,
      "name": "AaronLieb",
      "completion_day_level": {
        "1": {
          "1": {
            "star_index": 7004,
            "get_star_ts": 1669871420
          }
        }
      }
    },
    "621845": {
      "stars": 2,
      "completion_day_level": {
        "1": {
          "2": {
            "star_index": 4544,
            "get_star_ts": 1669871252
          },
          "1": {
            "get_star_ts": 1669871020,
            "star_index": 0
          }
        }
      },
      "name": "diamondburned",
      "local_score": 30,
      "global_score": 0,
      "id": 621845,
      "last_star_ts": 1669871252
    }
  },
  "owner_id": 715845
}`
