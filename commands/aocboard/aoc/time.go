package aoc

import "time"

// EST is the timezone for Eastern Time (no DST).
var EST = time.FixedZone("Eastern Time", int((-5 * time.Hour).Seconds()))

// DayDuration is one day, or 24 hours.
var DayDuration = 24 * time.Hour

func UntilMidnightEST(t time.Time) time.Duration {
	return t.
		Truncate(DayDuration).
		Add(DayDuration).In(EST).
		Sub(t)
}
