package aoc

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type (
	// Days start from 0, so 0-24
	DaysCompleted     []DayCompletedParts
	DayCompletedParts [2]*time.Time
)

func (d *DaysCompleted) UnmarshalJSON(b []byte) error {
	var days map[int]map[int]struct {
		TimeEpoch int64 `json:"get_star_ts"`
	}

	if err := json.Unmarshal(b, &days); err != nil {
		return errors.Wrap(err, "Failed to parse leaderboard")
	}

	completed := make(DaysCompleted, TotalDays)

	for day, parts := range days {
		for i := 1; i <= 2; i++ {
			if when, ok := parts[i]; ok {
				time := time.Unix(when.TimeEpoch, 0)
				completed[day-1][i-1] = &time
			}
		}
	}

	*d = completed

	return nil
}

func (d DaysCompleted) GetDay(day int) (DayCompletedParts, bool) {
	this := d[day+1]
	if this[0] == nil && this[1] == nil {
		return this, false
	}

	return this, true
}

func (d DaysCompleted) Total() (total int) {
	for _, parts := range d {
		if parts.Completed() {
			total++
		}
	}

	return
}

func (d DayCompletedParts) Completed() bool {
	if d[0] != nil && d[1] != nil {
		return true
	}

	return false
}
