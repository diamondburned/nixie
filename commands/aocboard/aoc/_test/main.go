package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	s, err := discordgo.New(os.Getenv("TOKEN"))
	if err != nil {
		panic(err)
	}

	var fields = make([]*discordgo.MessageEmbedField, 12)

	for i := 1; i <= 12; i++ {
		fields[i-1] = &discordgo.MessageEmbedField{
			Name:   fmt.Sprintf("%d. tdeoman", i),
			Value:  fmt.Sprintf("%d", (13-i)*824),
			Inline: true,
		}
	}

	s.ChannelMessageSendEmbed("361910177961738244", &discordgo.MessageEmbed{
		Author: &discordgo.MessageEmbedAuthor{
			Name: "Advent of Code Leaderboard",
		},
		Title:  "621845-4733049c",
		Color:  0x00cc00,
		Fields: fields,
	})
}
