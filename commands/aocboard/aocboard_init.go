package aocboard

import (
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/commands/aocboard/aoc"
)

const tokenHelp = `Please paste your Session token here.

To find the token on Chrome, do:
	1. Enter the Developer Console (Ctrl-Shift-C)
	2. Go to [Application]
	3. Go to Storage -> Cookies -> <https://adventofcode.com>
	4. Find the cookie named "session" and copy its value`

func (a *AOC) Init(
	msg *gateway.MessageCreateEvent, lb string, ch arguments.ChannelMention) (string, error) {

	aoc, err := aoc.New(time.Now().Year(), lb)
	if err != nil {
		return "", err
	}

	// We DM the user and wait for them to give us the token
	dm, err := a.Ctx.CreatePrivateChannel(msg.Author.ID)
	if err != nil {
		return "", errors.Wrap(err, "failed to get DM channel")
	}

	// Add a prompt before the message is sent.
	a.promptMu.Lock()
	a.prompts[dm.ID] = prompt{
		Settings: Settings{
			AOC:     *aoc,
			Guild:   msg.GuildID,
			Channel: ch.ID(),
		},
		time: unixTime(time.Now().Unix()),
	}
	a.promptMu.Unlock()

	_, err = a.Ctx.SendText(dm.ID, tokenHelp)
	if err != nil {
		// Remove the prompt before exiting.
		a.promptMu.Lock()
		delete(a.prompts, dm.ID)
		a.promptMu.Unlock()

		return "", errors.Wrap(err, "failed to DM secret prompt")
	}

	return msg.Author.Mention() + ", check your direct messages.", nil
}

func (a *AOC) dmCheck(msg *gateway.MessageCreateEvent) error {
	r, err := a.verifyDM(msg)

	if err == nil && r != "" {
		a.Ctx.SendText(msg.ChannelID, r)
		return nil
	}

	if err != nil && err != bot.Break {
		_, err = a.Ctx.SendText(msg.ChannelID, err.Error())
	}

	return err
}

func (a *AOC) verifyDM(msg *gateway.MessageCreateEvent) (string, error) {
	// Actually check that it's a DM. If it's from a guild, then we can
	// continue.
	if msg.GuildID.IsValid() {
		return "", nil
	}
	// Ignore messages from us to avoid feedback loops.
	if msg.Author.Bot {
		return "", bot.Break
	}

	a.promptMu.Lock()
	p, ok := a.prompts[msg.ChannelID]
	a.promptMu.Unlock()

	// The message is neither from a known channel nor a known guild. Break.
	if !ok {
		return "", bot.Break
	}

	cancel := a.idle(msg.ChannelID)
	defer cancel()

	p.AOC.Token = msg.Content

	// Construct an embed
	e, err := p.Settings.Embed()
	if err != nil {
		return "", errors.Wrap(err, "failed to construct embed, retry possible")
	}

	a.promptMu.Lock()
	delete(a.prompts, msg.ChannelID)
	a.promptMu.Unlock()

	// Send the embed
	m, err := a.Ctx.SendEmbed(p.Channel, *e)
	if err != nil {
		return "", errors.Wrap(err, "failed to sendembed to target channel")
	}

	// Save the settings
	p.Settings.Message = m.ID

	if err := a.node.Set(p.Guild.String(), p.Settings); err != nil {
		return "", errors.Wrap(err, "failed to save settings")
	}

	return "AOC leaderboard registered; check " + p.Channel.Mention() + ".", nil
}
