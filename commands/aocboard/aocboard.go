package aocboard

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/commands/aocboard/aoc"
	"gitlab.com/diamondburned/nixie/db"
)

// TODO: Add a channel, remove (or finalize) AOC on command

// // Used only for update
// var limiter = exmiddleware.UserCooldown(
// 	15*time.Minute,
// 	func(ctx *exrouter.Context) {
// 		ctx.Reply("Not yet...")
// 	},
// )

const help = `Usage:
	aoc update
	aoc init <leaderboard> <targetChannel>
	aoc finalize - freezes the leaderboard and stop updating
`

type unixTime = uint32

type AOC struct {
	Ctx  *bot.Context
	node db.Node

	lastUpdated  map[discord.GuildID]unixTime
	lastUpdateMu sync.Mutex

	prompts  map[discord.ChannelID]prompt
	promptMu sync.Mutex
}

type prompt struct {
	Settings
	time unixTime
}

func before(now time.Time, t unixTime, p time.Duration) bool {
	return time.Unix(int64(t), 0).Add(p).After(now)
}

func after(now time.Time, t unixTime, p time.Duration) bool {
	return !before(now, t, p)
}

const (
	UpdateCooldown = 2 * time.Minute
	PromptTimeout  = 30 * time.Minute
)

func New(node db.Node) *AOC {
	return &AOC{
		node:        node,
		lastUpdated: map[discord.GuildID]uint32{},
		prompts:     map[discord.ChannelID]prompt{},
	}
}

var _ bot.CanSetup = (*AOC)(nil)

func (aoc *AOC) Setup(sub *bot.Subcommand) {
	sub.Description = "Advent of Code leaderboard."
	sub.AddMiddleware("*", aoc.dmCheck)

	adminCheck := middlewares.AdminOnly(aoc.Ctx)
	sub.AddMiddleware(aoc.Finalize, adminCheck)
	sub.AddMiddleware(aoc.Init, adminCheck)
	sub.AddMiddleware(aoc.Update, func(msg *gateway.MessageCreateEvent) error {
		if aoc.canUpdate(msg.GuildID) || adminCheck(msg) == nil {
			return nil
		}
		return bot.Break
	})

	sub.Hide(aoc.Finalize)
	sub.Hide(aoc.Init)

	sub.ChangeCommandInfo(aoc.Finalize, "", "freezes the leaderboard and stop updating")

	init := sub.FindCommand(aoc.Init)
	init.Arguments[0].String = "leaderboard-code"
	init.Arguments[1].String = "#leaderboard-channel"

	aocPool.add(aoc)

	// Made a mistake before. This is to fix it. Remove me after deploying.
	aoc.node.Delete("0")
}

func (aoc *AOC) idle(ch discord.ChannelID) context.CancelFunc {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)

	// We don't care if this fails or not.
	go aoc.Ctx.Client.WithContext(ctx).Typing(ch)

	return cancel
}

func (aoc *AOC) updateAll() {
	var st Settings
	aoc.node.Each(&st, "", func(string) error {
		st.Update(aoc)
		return nil
	})
}

func (aoc *AOC) canUpdate(guild discord.GuildID) bool {
	now := time.Now()

	aoc.lastUpdateMu.Lock()
	defer aoc.lastUpdateMu.Unlock()

	u, ok := aoc.lastUpdated[guild]
	if ok {
		if time.Unix(int64(u), 0).Add(UpdateCooldown).After(now) {
			return false
		}
	}

	aoc.lastUpdated[guild] = unixTime(now.Unix())
	return true
}

func (aoc *AOC) Update(msg *gateway.MessageCreateEvent) (string, error) {
	var s Settings

	if err := aoc.node.Get(msg.GuildID.String(), &s); err != nil {
		return "", errors.Wrap(err, "failed to get AOC guild")
	}

	cancel := aoc.idle(msg.ChannelID)
	defer cancel()

	if err := s.Update(aoc); err != nil {
		return "", err
	}

	return "Updated successfully.", nil
}

func (aoc *AOC) Finalize(msg *gateway.MessageCreateEvent) error {
	var s Settings

	if err := aoc.node.Get(msg.GuildID.String(), &s); err != nil {
		return errors.Wrap(err, "failed to get AOC guild")
	}

	// Wipe asap.
	deleteError := aoc.node.Delete(msg.GuildID.String())

	m, err := aoc.Ctx.Message(s.Channel, s.Message)
	if err != nil {
		return errors.Wrap(err, "failed to find leaderboard message")
	}

	if len(m.Embeds) != 1 {
		return errors.Wrapf(err, "message has invalid embed count %d", len(m.Embeds))
	}

	// Set pill color to red
	m.Embeds[0].Color = 0xcc0000
	m.Embeds[0].Footer.Text = "Finalized"
	m.Embeds[0].Timestamp = discord.NowTimestamp()

	_, err = aoc.Ctx.EditEmbed(s.Channel, s.Message, m.Embeds[0])
	if err != nil {
		return errors.Wrap(err, "failed to edit old message")
	}

	return deleteError
}

type Settings struct {
	Guild   discord.GuildID
	Channel discord.ChannelID
	Message discord.MessageID

	AOC aoc.Session
}

func (s Settings) FatalEmbed(err error) *discord.Embed {
	return &discord.Embed{
		Author: &discord.EmbedAuthor{
			Name: "Advent of Code Leaderboard",
			URL:  aoc.URL,
			Icon: aoc.Favicon,
		},
		Footer: &discord.EmbedFooter{
			Text: "Updated",
		},
		Title:       s.AOC.Code,
		URL:         s.AOC.LeaderboardURL(),
		Color:       0xFF0000,
		Timestamp:   discord.NowTimestamp(),
		Description: "Error updating: " + err.Error(),
	}
}

func (s Settings) Update(aoc *AOC) error {
	e, err := s.Embed()
	if err != nil {
		e = s.FatalEmbed(err)
	}

	_, err = aoc.Ctx.EditEmbed(s.Channel, s.Message, *e)
	if err != nil {
		return errors.Wrap(err, "failed to edit old message")
	}

	return nil
}

func (s Settings) Embed() (*discord.Embed, error) {
	var latestDay = s.AOC.CalculateLatestDay()

	embed := discord.Embed{
		Author: &discord.EmbedAuthor{
			Name: "Advent of Code Leaderboard",
			URL:  aoc.URL,
			Icon: aoc.Favicon,
		},
		Footer: &discord.EmbedFooter{
			Text: "Updated",
		},
		Title:     s.AOC.Code,
		URL:       s.AOC.LeaderboardURL(),
		Color:     0x00CC00,
		Timestamp: discord.NowTimestamp(),
	}

	if latestDay == 0 {
		embed.Description = "It's not Advent of Code, yet!"
		return &embed, nil
	}

	// Grab the leaderboard
	l, err := s.AOC.GetLeaderboard()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get leaderboard")
	}

	// Start creating the AOC leaderboard
	embed.Fields = formatAOCLeaderboard(s.AOC, l, latestDay)

	switch {
	case len(embed.Fields) == 0:
		embed.Description = "Nobody is here :("
	default:
		embed.Description = fmt.Sprintf(
			"[Day %d / %d](https://adventofcode.com/%d/day/%d)",
			latestDay, aoc.TotalDays, s.AOC.Year, latestDay,
		)
	}

	return &embed, nil
}

func formatAOCLeaderboard(
	s aoc.Session,
	lb *aoc.Leaderboard, latestDay int) []discord.EmbedField {

	ranks := lb.SortMemberScores()

	var fields = make([]discord.EmbedField, 0, 12)

	for i := 0; i < 12; i++ {
		// Already have fields for everyone in the leaderboard.
		if i >= len(ranks) {
			break
		}

		var name = ranks[i].Name
		if name == "" {
			name = fmt.Sprint("#", ranks[i].ID)
		}

		fields = append(fields, discord.EmbedField{
			Name: fmt.Sprintf("%d. %s", i+1, name),
			Value: fmt.Sprintf(
				"Completed %d/%d\n⭐ %d (%d)",
				ranks[i].DaysCompleted.Total(), latestDay,
				ranks[i].Stars,
				ranks[i].LocalScore,
			),
			Inline: true,
		})
	}

	return fields
}
