package aocboard

import (
	"sync"
	"time"

	"gitlab.com/diamondburned/clocker"
)

const (
	UpdatePeriod  = 10 * time.Minute
	CleanupPeriod = 30 * time.Minute
)

var aocPool aocManagers

type aocManagers struct {
	managers []aocManager
	mutex    sync.Mutex
}

func (m *aocManagers) add(aoc *AOC) {
	m.mutex.Lock()
	m.managers = append(m.managers, aocManager{aoc, make(chan struct{}, 1)})
	m.mutex.Unlock()
}

func (m *aocManagers) each(fn func(aocManager)) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	for _, manager := range m.managers {
		fn(manager)
	}
}

type aocManager struct {
	*AOC
	updating chan struct{}
}

func (m *aocManager) queueUpdate() {
	select {
	case m.updating <- struct{}{}:

	default:
		return
	}

	go func() {
		m.updateAll()
		<-m.updating
	}()
}

func init() {
	go func() {
		cleanupTicker := time.NewTicker(CleanupPeriod)
		defer cleanupTicker.Stop()

		updateTicker := clocker.NewTicker(UpdatePeriod)
		defer updateTicker.Stop()

		for {
			select {
			case <-updateTicker.C:
				aocPool.each(func(m aocManager) { m.queueUpdate() })
			case now := <-cleanupTicker.C:
				aocPool.each(func(m aocManager) { m.cleanup(now) })
			}
		}
	}()
}

func (aoc *AOC) cleanup(now time.Time) {
	aoc.lastUpdateMu.Lock()
	for guild, t := range aoc.lastUpdated {
		if after(now, t, UpdateCooldown) {
			delete(aoc.lastUpdated, guild)
		}
	}
	aoc.lastUpdateMu.Unlock()

	aoc.promptMu.Lock()
	for channel, prompt := range aoc.prompts {
		if after(now, prompt.time, PromptTimeout) {
			delete(aoc.prompts, channel)
		}
	}
	aoc.promptMu.Unlock()
}
