package roles

import (
	"fmt"
	"sort"
	"strings"

	"github.com/diamondburned/arikawa/v2/api"
	"github.com/diamondburned/arikawa/v2/bot"
	"github.com/diamondburned/arikawa/v2/bot/extras/arguments"
	"github.com/diamondburned/arikawa/v2/bot/extras/middlewares"
	"github.com/diamondburned/arikawa/v2/discord"
	"github.com/diamondburned/arikawa/v2/gateway"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/nixie/db"
	"gitlab.com/diamondburned/nixie/internal/dctools"
)

type Roles struct {
	Ctx  *bot.Context
	node db.Node
}

func New(node db.Node) *Roles {
	return &Roles{
		node: node,
	}
}

func (r *Roles) Setup(sub *bot.Subcommand) {
	sub.Description = "Role manager for members."
	sub.AddMiddleware("*", middlewares.GuildOnly(r.Ctx))
	sub.AddMiddleware("Reset, Split, Name, CanGive, Update", middlewares.AdminOnly(r.Ctx))

	sub.FindCommand("Toggle").Arguments[0].String = "name"
	sub.FindCommand("Toggle").Description = "Toggle a role for yourself."

	sub.FindCommand("List").Description = "List all assignable roles."

	sub.SetPlumb(r.Toggle)
}

func (r *Roles) Reset(m *gateway.MessageCreateEvent) error {
	if err := r.guildNode(m.GuildID).Drop(); err != nil {
		return errors.Wrap(err, "Failed to drop your guild")
	}
	_, err := r.Ctx.SendMessage(m.ChannelID, "Reset guild.", nil)
	return err
}

func (r *Roles) Split(m *gateway.MessageCreateEvent, header string) error {
	role := dctools.SearchRole(r.Ctx, m.GuildID, header)
	if role == nil {
		return errors.New("Can't find role")
	}

	region := RoleRegion{
		RoleHeader: role.ID,
		RoleName:   role.Name,
		RegionName: role.Name,
		Position:   int32(role.Position),
	}

	err := r.guildNode(m.GuildID).Set(region.RoleHeader.String(), region)
	if err != nil {
		return errors.Wrap(err, "Failed to split roles")
	}

	_, err = r.Ctx.SendMessage(m.ChannelID, "Split role "+role.Name, nil)
	return err
}

func (r *Roles) Name(m *gateway.MessageCreateEvent, h, n string) (string, error) {
	var region = r.findRegion(m.GuildID, h)
	if region == nil {
		return "", errors.New("Region not found")
	}

	region.RegionName = n

	var node = r.guildNode(m.GuildID)
	if err := node.Set(region.RoleHeader.String(), region); err != nil {
		return "", errors.Wrap(err, "Can't save role")
	}

	return "Renamed role header", nil
}

func (r *Roles) CanGive(
	m *gateway.MessageCreateEvent,
	header string, allow bool) (string, error) {

	var region = r.findRegion(m.GuildID, header)
	if region == nil {
		return "", errors.New("Region not found")
	}

	var msg string

	if allow {
		region.AllowGive = true
		msg = "Role can now be assigned."
	} else {
		region.AllowGive = false
		msg = "Role can no longer be assigned."
	}

	var node = r.guildNode(m.GuildID)
	if err := node.Set(region.RoleHeader.String(), region); err != nil {
		return "", errors.Wrap(err, "Can't save role")
	}

	return msg, nil
}

// Update cycles through all regions and update their children
// IDs. Run this after you've made changes to the roles.
func (r *Roles) Update(m *gateway.MessageCreateEvent) error {
	node := r.guildNode(m.GuildID)

	var regions []*RoleRegion
	if err := node.All(&regions, ""); err != nil {
		return errors.Wrap(err, "Can't get regions")
	}

	sort.Slice(regions, func(i, j int) bool {
		return regions[i].Position > regions[j].Position
	})

	roles, err := r.Ctx.Roles(m.GuildID)
	if err != nil {
		return err
	}

	// Sort role positions
	sort.Slice(roles, func(i, j int) bool {
		return roles[i].Position > roles[j].Position
	})

	var j int
	var sweep bool

	for i := 0; i < len(regions); i++ {
		r := regions[i]
		r.ChildrenIDs = []discord.RoleID{}

		//  ignore @everyone v
		for ; j < len(roles)-1; j++ {
			// This should only be true once ever.
			if r.RoleHeader == roles[j].ID {
				sweep = true
				continue
			}

			// This checks the next roles as we sweep
			if i+1 < len(regions) {
				if regions[i+1].RoleHeader == roles[j].ID {
					break
				}
			}

			if sweep {
				r.ChildrenIDs = append(r.ChildrenIDs, roles[j].ID)
			}
		}

		// Save each region
		if err := node.Set(r.RoleHeader.String(), r); err != nil {
			return errors.Wrap(err, "Failed to save for role "+r.RegionName)
		}
	}

	_, err = r.Ctx.SendMessage(m.ChannelID, "Updated list.", nil)
	return err
}

func (r *Roles) Toggle(
	m *gateway.MessageCreateEvent, name arguments.Joined) (*api.SendMessageData, error) {

	if name == "" {
		return nil, errors.New("Missing role name. Usage: `toggle <name>`")
	}

	// Get and find roles
	roles, err := r.Ctx.Roles(m.GuildID)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get roles")
	}

	role, matches := r.matchRoles(m.GuildID, roles, string(name))
	if role == nil {
		if len(matches) == 0 {
			return nil, errors.New("Role not found.")
		}

		b := strings.Builder{}
		b.WriteString("Role not found; did you mean ")

		for i, match := range matches {
			// We can use the ID here as long as we tell Discord to not actually
			// parse any of the mentions as actual mentions.
			fmt.Fprintf(&b, "<@&%d>", match.ID)

			// Stop if the string is already too long.
			if b.Len() > 256 {
				break
			}

			// Append the "or" separator to make a proper English sentence.
			if i < len(matches)-1 {
				b.WriteString(" or ")
			}
		}

		b.WriteString("?")

		return &api.SendMessageData{
			Content: b.String(),
			AllowedMentions: &api.AllowedMentions{
				// Do not mention anyone.
				Parse: []api.AllowedMentionType{},
			},
		}, nil
	}

	member, err := r.Ctx.Member(m.GuildID, m.Author.ID)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get member")
	}

	var have bool

	for _, r := range member.RoleIDs {
		if r == role.ID {
			have = true
			break
		}
	}

	var msg string
	if have {
		msg = "Removed"
		err = r.Ctx.RemoveRole(m.GuildID, m.Author.ID, role.ID)
	} else {
		msg = "Added"
		err = r.Ctx.AddRole(m.GuildID, m.Author.ID, role.ID)
	}

	if err != nil {
		return nil, errors.Wrap(err, "Failed to change roles for you")
	}

	return &api.SendMessageData{
		Content: fmt.Sprintf("%s role for %s", msg, m.Author.Mention()),
		// Just for safety measures, we're doing this.
		AllowedMentions: &api.AllowedMentions{
			Parse: []api.AllowedMentionType{},
			Users: []discord.UserID{m.Author.ID},
		},
	}, nil
}

func (r *Roles) matchRoles(
	guildID discord.GuildID,
	roles []discord.Role, name string) (found *discord.Role, suggests []discord.Role) {

	name = strings.ToLower(name)

	// Get the regions
	var regions []RoleRegion
	if err := r.guildNode(guildID).All(&regions, ""); err != nil {
		// If there are no regions, then we won't have anything to match.
		return nil, nil
	}

	for _, r := range roles {
		var rolename = strings.ToLower(r.Name)

		// If the rolename matches AND the role is in the region, then use it
		// and break the loop.
		if rolename == name && RegionsContain(regions, r.ID) {
			found = &r
			break
		}

		// Skip if we already have too many matches.
		if len(suggests) > 10 {
			continue
		}

		// If the role roughly matches the name, the actual name is long enough,
		// and the role is in the allowed regions, then we should "match" it.
		if len(name) > 2 && strings.Contains(rolename, name) && RegionsContain(regions, r.ID) {
			suggests = append(suggests, r)
		}
	}

	// Early exit if we've found the role.
	return
}

func (r *Roles) List(m *gateway.MessageCreateEvent) error {
	var regions []*RoleRegion
	if err := r.guildNode(m.GuildID).All(&regions, ""); err != nil {
		return errors.Wrap(err, "Can't get regions")
	}

	p := dctools.DefaultPaginator(r.Ctx.State, *m)

	var embeds = make([]discord.Embed, 0, len(regions))

	for _, rg := range regions {
		if rg.AllowGive == false {
			continue
		}

		e := discord.Embed{
			Color: 0x34be5b,
			Title: rg.RegionName,
		}

		for _, id := range rg.ChildrenIDs {
			// Mention the role. This won't actually ping, but it would give the
			// roles colors.
			e.Description += "<@&" + id.String() + ">\n"
		}

		embeds = append(embeds, e)
	}

	if len(embeds) == 0 {
		return errors.New("No regions found.")
	}

	p.Add(embeds...)
	p.SetPageFooters()
	return p.Spawn()
}

func (r *Roles) guildNode(guildID discord.GuildID) db.Node {
	return r.node.Node(guildID.String())
}

func (r *Roles) findRegion(guildID discord.GuildID, name string) *RoleRegion {
	var rg *RoleRegion
	var found bool

	r.node.Node(guildID.String()).Each(&rg, "",
		func(string) error {
			if rg.RoleName == name || rg.RoleHeader.String() == name {
				found = true
				return db.EachBreak
			}
			return nil
		},
	)

	if !found {
		return nil
	}

	return rg
}

// RoleRegion is the struct for a role region.
type RoleRegion struct {
	RoleHeader discord.RoleID
	RoleName   string
	RegionName string
	Position   int32

	AllowGive bool

	ChildrenIDs []discord.RoleID
}

func RegionsContain(regions []RoleRegion, role discord.RoleID) bool {
	for _, r := range regions {
		if r.AllowGive == false {
			continue
		}

		for _, id := range r.ChildrenIDs {
			if id == role {
				return true
			}
		}
	}
	return false
}
