package db

import (
	"testing"

	badger "github.com/dgraph-io/badger/v2"
)

func NewTestKV(t *testing.T) *KV {
	b, err := NewKV(badger.DefaultOptions("").WithInMemory(true))

	if err != nil {
		t.Fatal("Failed to create a new kv:", err)
	}

	return b
}
