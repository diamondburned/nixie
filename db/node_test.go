package db

import (
	"testing"

	"github.com/dgraph-io/badger/v2"
	"github.com/pkg/errors"
)

func TestNodePrefix(t *testing.T) {
	kv := NewTestKV(t)
	n := kv.Node("node")
	n = n.Node("second_node")

	if p := n.Prefix(); p != "node:::second_node:::" {
		t.Fatal("Unexpected prefix:", p)
	}
}

func TestNodeGetSet(t *testing.T) {
	kv := NewTestKV(t)
	n := kv.Node("node")

	if err := n.Set("thing", "Arikawa Hime"); err != nil {
		t.Fatal("Failed to set:", err)
	}

	// Predict what the key probably is
	err := kv.DB.View(func(tx *badger.Txn) error {
		i, err := tx.Get([]byte("node:::thing"))
		if err != nil {
			return err
		}

		v, err := i.ValueCopy(nil)
		if err != nil {
			return errors.Wrap(err, "Failed to copy key")
		}

		if len(v) == 0 {
			return errors.New("value is empty")
		}

		return nil
	})

	if err != nil {
		t.Fatal("Error in Badger store:", err)
	}

	// Actually check the value
	var str string
	if err := n.Get("thing", &str); err != nil {
		t.Fatal("Failed to get:", err)
	}

	if str != "Arikawa Hime" {
		t.Fatal("Unexpected string:", str)
	}
}

func TestNodeAll(t *testing.T) {
	kv := NewTestKV(t)
	n := kv.Node("himegoto").Node("character_names")

	/* Slice of strings */

	var items = []string{
		"Hime Arikawa",
		"18-kin",
		"Unko",
		"Hiro",
		"Mitsunaga Oda",
		"Servant No. 1",
		"Albertina II",
		"Tadokoro",
		"Kaguya Arikawa",
	}
	var length = len(items)

	for _, item := range items {
		if err := n.Set("Himegoto - "+item, item); err != nil {
			t.Fatal("Can't set item", item+":", err)
		}
	}

	var results []string

	if err := n.All(&results, "Himegoto - "); err != nil {
		t.Fatal("Can't get all:", err)
	}

	if length != len(results) {
		t.Fatal("Unexpected length:", len(results))
	}

	/* Slice of pointers to structs */

	n = kv.Node("himegoto").Node("characters")

	type thing struct {
		Franchise string
		Character string
	}
	var things = make([]*thing, len(items))

	for i := range things {
		things[i] = &thing{
			Franchise: "Himegoto",
			Character: items[i],
		}
	}

	for _, thing := range things {
		if err := n.Set(thing.Character, thing); err != nil {
			t.Fatal("Can't set thing:", err)
		}
	}

	var resultThings []*thing

	if err := n.All(&resultThings, ""); err != nil {
		t.Fatal("Can't get all:", err)
	}

	if length != len(resultThings) {
		t.Fatal("Unexpected length:", len(resultThings))
	}

	if resultThings[0].Franchise != "Himegoto" {
		t.Fatal(
			"Unexpected resultThings[0].Franchise:",
			resultThings[0].Franchise,
		)
	}
}

func TestNodeEach(t *testing.T) {
	kv := NewTestKV(t)
	n := kv.Node("good characters")

	type Character struct {
		Name  string
		Anime string
	}

	var characters = []*Character{
		{"Saika Totsuka", "OreGairu"},
		{"Hime Arikawa", "Himegoto"},
		{"Kaguya Arikawa", "Himegoto"},
		{"Astolfo", "Fate/Grand Order"},
	}

	for _, ch := range characters {
		if err := n.Set(ch.Name, ch); err != nil {
			t.Fatal("Failed to set:", err)
		}
	}

	var himegotoCharacters []*Character

	var ch = &Character{}
	n.Each(ch, "", func(k string) error {
		if ch.Anime == "Himegoto" {
			character := *ch
			himegotoCharacters = append(himegotoCharacters, &character)
		}
		return nil
	})

	if len(himegotoCharacters) != 2 {
		t.Fatal("Unexpected length:", len(himegotoCharacters))
	}

	if himegotoCharacters[0].Anime != "Himegoto" {
		t.Fatal(
			"Unexpected himegotoCharacters[0] value:", himegotoCharacters[0])
	}
}

func TestNodeDrop(t *testing.T) {
	kv := NewTestKV(t)
	n := kv.Node("good characters")

	type Character struct {
		Name  string
		Anime string
	}

	var characters = []*Character{
		{"Saika Totsuka", "OreGairu"},
		{"Hime Arikawa", "Himegoto"},
		{"Kaguya Arikawa", "Himegoto"},
		{"Astolfo", "Fate/Grand Order"},
	}

	for _, ch := range characters {
		if err := n.Set(ch.Name, ch); err != nil {
			t.Fatal("Failed to set:", err)
		}
	}

	if err := n.Drop(); err != nil {
		t.Fatal("Failed to drop:", err)
	}

	var results []Character

	if err := n.All(&results, ""); err != nil {
		t.Fatal("Can't get all:", err)
	}

	if len(results) > 0 {
		t.Fatal("Node is not dropped")
	}
}
