package db

import (
	"sync"

	"github.com/dgraph-io/badger/v2"
	"github.com/pkg/errors"
)

const Delimiter = ":::"

type KV struct {
	Marshalers
	DB *badger.DB

	mu sync.RWMutex
}

func NewKVFile(path string) (*KV, error) {
	opt := badger.DefaultOptions(path)
	opt.WithLogger(&Log{})
	opt.WithMaxCacheSize(4 * 1024 * 1024) // 4MB
	opt.WithZSTDCompressionLevel(1)
	return NewKV(opt)
}

func NewKV(opts badger.Options) (*KV, error) {
	b, err := badger.Open(opts)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to open a Badger DB")
	}

	return KVWithDB(b), nil
}

func KVWithDB(db *badger.DB) *KV {
	return &KV{
		DB:         db,
		Marshalers: NewGob(),
	}
}

func (kv *KV) Node(name string) Node {
	if name == "" {
		panic("Node name can't be empty")
	}

	return Node{
		prefixes: [][]byte{[]byte(name)},
		kvdb:     kv,
	}
}

func (kv *KV) Close() error {
	kv.mu.Lock()
	defer kv.mu.Unlock()

	return kv.DB.Close()
}
