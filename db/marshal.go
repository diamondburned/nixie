package db

import (
	"bytes"
	"encoding/gob"
	"sync"
)

type Marshalers interface {
	Marshal(interface{}) ([]byte, error)
	Unmarshal([]byte, interface{}) error
}

type Gob struct {
	bufPool sync.Pool
}

var _ Marshalers = (*Gob)(nil)

func NewGob() *Gob {
	return &Gob{
		bufPool: sync.Pool{
			New: func() interface{} {
				return new(bytes.Buffer)
			},
		},
	}
}

func (g *Gob) Marshal(v interface{}) ([]byte, error) {
	buf := g.bufPool.Get().(*bytes.Buffer)
	defer g.bufPool.Put(buf)

	defer buf.Reset()

	if err := gob.NewEncoder(buf).Encode(v); err != nil {
		return nil, err
	}

	return cpy(buf.Bytes()), nil
}

func (g *Gob) Unmarshal(b []byte, v interface{}) error {
	return gob.NewDecoder(bytes.NewReader(b)).Decode(v)
}

func cpy(src []byte) []byte {
	dst := make([]byte, len(src))
	copy(dst, src)
	return dst
}
