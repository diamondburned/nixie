package db

import "log"

type Log struct{}

// noop
func (*Log) Infof(string, ...interface{}) {}

// noop
func (*Log) Debugf(string, ...interface{}) {}

func (*Log) Warningf(f string, v ...interface{}) {
	log.Printf("Badger warning: "+f, v...)
}
func (*Log) Errorf(f string, v ...interface{}) {
	log.Printf("Badger error: "+f, v...)
}
